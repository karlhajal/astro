/**
 * @file mainwindow.h
 * @brief The MainWindow class
 *
 * This class is the main menu of our game,
 * From here one can select the scenario they want to play.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDirIterator>
#include <QMainWindow>
#include "storynode.h"

namespace Ui {
    class MainWindow;
}


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0, QWidget* _parentWindow = nullptr, QString state = "", QString user = ""); /**< MainWindow Constructor */
    ~MainWindow(); /**< MainWindow Destructor */
    void load_scenario(QString scenario, int offset, QVector<int>& values); /**< Loads a scenario from a text file */
    QString user;

private slots:
    void on_pushButton_3_clicked(); /**< Loads the first scenario */

    void on_pushButton_clicked(); /**< Closes the window */

    void on_pushButton_4_clicked(); /**< Loads the second scenario */

    void on_pushButton_2_clicked();

private:
    Ui::MainWindow *ui; /**< A pointer to the forward declared class that Qt auto-generated from the ui file */
    StoryNode *story_node; /**< The root of our directed acyclic story graph */
    QWidget* parentWindow;
};

#endif // MAINWINDOW_H
