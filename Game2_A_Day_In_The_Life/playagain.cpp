/**
 * @file playagain.cpp
 * @brief PlayAgain class method definitions
 *
 */

#include "playagain.h"
#include "ui_playagain.h"
#include <string>

/**
* @brief PlayAgain constructor
*
* Sets up the UI and presents feedback to the player regarding each value based on their performance.
* Also gives the player the option to play again or go back to the main menu
*
*/
PlayAgain::PlayAgain(QWidget *_main_menu, QWidget *parent, int _happiness, int _adventure,
                     int _acceptance, int _persistence, int _decisiveness) :
    QDialog(parent),
    ui(new Ui::PlayAgain)
{
    srand(time(0));
    main_menu = _main_menu;
    ui->setupUi(this);
    parent_widget = parent;
    happiness = _happiness;
    adventure = _adventure;
    persistence = _persistence;
    acceptance = _acceptance;
    decisiveness = _decisiveness;

    ui->label_2->setText(QString::fromStdString(std::to_string(happiness)));
    ui->label_4->setText(QString::fromStdString(std::to_string(adventure)));
    ui->label_6->setText(QString::fromStdString(std::to_string(acceptance)));
    ui->label_8->setText(QString::fromStdString(std::to_string(persistence)));
    ui->label_12->setText(QString::fromStdString(std::to_string(decisiveness)));

    if(happiness < 50) {
        ui->hapinessVerdict->setText("You have demonstrated a lack of happiness, here is a video to help: \n"
                                     "<a href=\"" + happyVideos[rand() % happyVideos.size()] + "\"> Click here for the video </a>");
    }
    else if(happiness < 75) {
        ui->hapinessVerdict->setText("You are a fairly happy person.");
    }else {
        ui->hapinessVerdict->setText("You are a perfectly happy person!");
    }

    if(adventure < 50) {
        ui->adventureVerdict->setText("You have demonstrated a lack of adventure, here is a video to help: \n"
                                      "<a href=\"" + adventureVideos[rand() % adventureVideos.size()] +  "\"> Click here for the video </a>");
    } else if(adventure < 75){
        ui->adventureVerdict->setText("You are fairly adventurous.");
    }else {
        ui->adventureVerdict->setText("You have a good spirit of adventure!");
    }

    if(acceptance < 50) {
        ui->acceptanceVerdict->setText("You have demonstrated a lack of acceptance, here is a video to help: \n"
                                       "<a href=\"" + acceptanceVideos[rand() % acceptanceVideos.size()] +  "\"> Click here for the video </a>");
    } else if(acceptance < 75){
        ui->acceptanceVerdict->setText("You are a fairly acceptant and tolerant person.");
    }else {
        ui->acceptanceVerdict->setText("You are a very acceptant and tolerant person!");
    }

    if(persistence < 50) {
        ui->persistenceVerdict->setText("You have demonstrated a lack of persistence, here is a video to help: \n"
                                        "<a href=\"" + persistenceVideos[rand() % persistenceVideos.size()] +  "\"> Click Here for the video </a>");
    } else if(persistence < 75){
        ui->persistenceVerdict->setText("You are a fairly persistent and resilient person.");
    }else {
        ui->persistenceVerdict->setText("You are a very persistent and resilient person!");
    }

    if(decisiveness < 50) {
        ui->decisivenessVerdict->setText("You have demonstrated a lack of decisiveness, here is a video to help: \n"
                                        "<a href=\"" + decisivenessVideos[rand() % decisivenessVideos.size()] +  "\"> Click Here for the video </a>");
    } else if(persistence < 75){
        ui->decisivenessVerdict->setText("You are a fairly decisive person.");
    }else {
        ui->decisivenessVerdict->setText("You are a very decisive person and you know what you want!");
    }


    makeHyperLink(ui->adventureVerdict);
    makeHyperLink(ui->acceptanceVerdict);
    makeHyperLink(ui->hapinessVerdict);
    makeHyperLink(ui->persistenceVerdict);
    makeHyperLink(ui->decisivenessVerdict);
}

/**
* @brief PlayAgain destructor
*
* Deletes the UI
*
*/
PlayAgain::~PlayAgain()
{
    delete ui;
}

/**
* @brief Turns a QLabel's text into a hyperlink
*
*
*/
void PlayAgain::makeHyperLink(QLabel * myLabel) {
    myLabel->setTextFormat(Qt::RichText);
    myLabel->setTextInteractionFlags(Qt::TextBrowserInteraction);
    myLabel->setOpenExternalLinks(true);
}

/**
* @brief Restarts the scenario when a button is pressed
*
*
*/
void PlayAgain::on_buttonBox_accepted()
{
    this->hide();
    parent_widget->show();
}

/**
* @brief Goes back to main menu when a button is pressed
*
*
*/
void PlayAgain::on_buttonBox_rejected()
{
    this->hide();
    main_menu->show();
}
