/**
* \file sprite.h
* \brief Sprite class
*
* Used to implement the sprite animations
*/

#ifndef SPRITE_H
#define SPRITE_H

#include <QObject>
#include <QGraphicsItem>
#include <QTimer>
#include <QPixmap>

class Sprite : public QObject
{
    Q_OBJECT
public:
    explicit Sprite(QObject *parent = 0, QString spritesheet_location = "", int sprite_width = 0, int sprite_height = 0, int scalewidth = 0, int scaleheight = 0, int frames = 0);

signals:

public slots:

private slots:

    void nextFrame();   //!< Slot for turning images into QPixmap

public:

    QPixmap getPixmap(); //!< Gets the object's pixmap
    QRectF boundingRect() const; //!< Gets the object's bounding Rectangle

private:

    QTimer *timer;      // Timer for turning images into QPixmap
    QPixmap *spriteImage;   // In this QPixmap object will be placed sprite
    int currentFrame;   // Coordinates X, which starts the next frame of the sprite
    int animation_speed = 100; //25ms animation speed
    int sprite_width, sprite_height, scale_width, scale_height, nbframes;
};

#endif // SPRITE_H
