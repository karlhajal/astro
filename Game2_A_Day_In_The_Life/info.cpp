/**
 * @file info.cpp
 * @brief Info class method definitions
 *
 */

#include "info.h"
#include "ui_info.h"

Info::Info(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Info)
{
    ui->setupUi(this);
    parent_widget = parent;
}

Info::~Info()
{
    delete ui;
}



void Info::on_pushButton_clicked()
{
    this->hide();
    parent_widget->show();
}
