/**
* \file signUp.cpp
* \brief contains signUp class method definitions
*
*/

#include "signUp.h"


/**
* \brief signUp constructor
*
* Instantiates all objects, calls the methods to set them up in a grid layout first and then a vertical layout, and finally connects buttons to slots
*
*/
signUp::signUp(QWidget *parent, QWidget *_parentWindow) : QWidget(parent)
{
    setFixedSize(800,600);
    parentWindow = _parentWindow;
    FirstName = new QLabel("First Name *");
    LastName = new QLabel("Last Name *");
    Email = new QLabel("Email *");
    Username = new QLabel("Username *");
    Password = new QLabel("Password *");
    ConfirmPassword = new QLabel("Confirm Password *");
    Age = new QLabel("Age");
    Gender = new QLabel("Gender");
    Reset = new QPushButton("Reset");
    Submit = new QPushButton("Submit");
    back = new QPushButton("Back");
    LFirstName = new QLineEdit;
    LLastName = new QLineEdit;
    LEmail = new QLineEdit;
    LPassword = new QLineEdit; LPassword->setEchoMode(QLineEdit::Password);
    LUsername = new QLineEdit;
    LConfirmPassword = new QLineEdit; LConfirmPassword->setEchoMode(QLineEdit::Password);
    SBAge = new QSpinBox;
    Male = new QRadioButton("Male");
    Female = new QRadioButton("Female");
    VBL = new QVBoxLayout;
    GL = new QGridLayout;

    setGridLayout();
    setVerticalLayout();

    connect(back, SIGNAL(clicked(bool)), this, SLOT(goBack()));
    connect(Reset, SIGNAL(clicked()), this, SLOT(resetForm()));
    connect(Submit, SIGNAL(clicked()), this, SLOT(submitForm()));
}

/**
* \brief Reset form
*
* Reset all form objects to initial values
*
*/
void signUp::resetForm() {
    LFirstName->setText("");
    LLastName->setText("");
    LEmail->setText("");
    LUsername->setText("");
    LPassword->setText("");
    LConfirmPassword->setText("");
    Male->setAutoExclusive(false);
    Male->setChecked(false);
    Male->setAutoExclusive(true);
    Female->setAutoExclusive(false);
    Female->setChecked(false);
    Female->setAutoExclusive(true);
    SBAge->setValue(0);
}

/**
* \brief Go back to previous menu
*
* Goes pack to the parent window
*
*/
void signUp::goBack() {
    this->hide();
    parentWindow->show();
}

/**
* \brief Submit current form
*
*
*
*/

bool signUp::checkFormEmpty() {
    return this->LUsername->text().size() != 0
            && this->LPassword->text().size() != 0
            && this->LFirstName->text().size() != 0
            && this->LLastName->text().size() != 0
            && this->Email->text().size() != 0
            && this->LConfirmPassword->text().size() != 0;
}

bool signUp::checkPasswordsMatch() {
    return this->LConfirmPassword->text() == this->LPassword->text();
}

bool signUp::checkPasswordValid() {
    bool uppercase = false, digit = false;
    for(char a = 'A'; a <= 'Z'; a++) {
        QChar current_char(a);
        if(this->LPassword->text().contains(current_char)) {
            uppercase = true;
            break;
        }
    }

    for(char a = '0'; a <= '9'; a++) {
        QChar current_char(a);
        if(this->LPassword->text().contains(current_char)) {
            digit = true;
            break;
        }
    }

    return uppercase && digit;
}

void signUp::submitForm() {

    if(!checkFormEmpty()) {
        QMessageBox *bx = new QMessageBox();
        bx->setText("Please fill the required fields");
        bx->exec();
        return;
    }


    if(!checkPasswordValid()) {
        QMessageBox *bx = new QMessageBox();
        bx->setText("Password must contain at least one upper case character and one digit");
        bx->exec();
        return;
    }

    if(!checkPasswordsMatch()) {
        QMessageBox *bx = new QMessageBox();
        bx->setText("Passwords much match");
        bx->exec();
        return;
    }

    std::ofstream outFile("users.txt", std::ios_base::app);
    outFile << this->LUsername->text().toStdString() << ":" << this->LPassword->text().toStdString() << '\n';


    this->hide();
    parentWindow->show();
}

/**
* \brief Sets all objects into a grid layout
*
*
*/
void signUp::setGridLayout() {
    QVBoxLayout *temp = new QVBoxLayout;
    QGroupBox * RadioGrp = new QGroupBox;
    temp->addWidget(Male);
    temp->addWidget(Female);
    RadioGrp->setLayout(temp);

    GL->addWidget(FirstName, 0, 0);
    GL->addWidget(LastName, 0, 1);
    GL->addWidget(LFirstName, 1, 0);
    GL->addWidget(LLastName, 1, 1);
    GL->addWidget(Email, 2, 0);
    GL->addWidget(Username, 2, 1);
    GL->addWidget(LEmail, 3, 0);
    GL->addWidget(LUsername, 3, 1);
    GL->addWidget(Password, 4, 0);
    GL->addWidget(ConfirmPassword, 4, 1);
    GL->addWidget(LPassword, 5, 0);
    GL->addWidget(LConfirmPassword, 5, 1);
    GL->addWidget(Age, 6, 0);
    GL->addWidget(Gender, 6, 1);
    GL->addWidget(SBAge, 7, 0);
    GL->addWidget(RadioGrp, 7, 1);
    upperGrp->setLayout(GL);
}

/**
* \brief Sets grid and the rest of objects in a vertical layout
*
*
*/
void signUp::setVerticalLayout() {
    VBL->addWidget(upperGrp);
    VBL->addWidget(Reset);
    VBL->addWidget(Submit);
    VBL->addWidget(back);
    this->setLayout(VBL);
}
