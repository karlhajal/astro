/**
* \file shalimoneplayer.cpp
* \brief contains ShalimonePlayer class method definitions
*
*/

#include "shalimoneplayer.h"

/**
* \brief ShalimonePlayer constructor
*
* Initializes the score and reads the highest score previously read by the player
* Sets the art for the player
* Initializes physics for the jump
* Connects the movement timer to the Update slot
*
*/

ShalimonePlayer::ShalimonePlayer(QObject *_parent, QWidget* parent, QWidget* _framework) : QObject(_parent)
{
    this->parent = parent;
    framework = _framework;
    current_score = 0;
    high_score = 0;
    QFile ifile("highscore.txt");
    if (!ifile.open(QIODevice::ReadWrite | QIODevice::Text))
        return;
    QString content = ifile.readAll();
    high_score = content.toInt();
    ifile.close();
    height = PLAYER_SIZE;

    this->setPixmap((QPixmap(":/images/jumping.png")).copy(0,0,40,40).scaled(height,height));
    play_again_window = new ShalimonePlayAgain(parent,framework);
    play_again_window->hide();
    connect(play_again_window, SIGNAL(resetGame()), this, SLOT(newGame()));
    dx = 0;
    dy = 0;

    horizontal_speed = 3;

    gravity = 0.3;
    vertical_acceleration = 0;

    jump_speed = 14;
    grounded = true;
    movementTimer = new QTimer();
    connect(movementTimer,SIGNAL(timeout()), this, SLOT(Update()));
    movementTimer->start(MOVEMENT_TIMER_FREQ);
    ResetKeys();

    InitializeSprites();

}


void ShalimonePlayer::newGame() {
    emit resetScene();
}

/**
* \brief Resets the KeyBindings to false
*
*
*/
void ShalimonePlayer::ResetKeys(){
    KeyBindings[down] = 0;
    KeyBindings[right] = 0;
    KeyBindings[left] = 0;
    KeyBindings[jump] = 0;
}

/**
* \brief Reads key presses and updates key bindings accordingly
*
*
*/
void ShalimonePlayer::keyPressEvent(QKeyEvent *event){
    if(event->key() != jump || grounded)
        KeyBindings[event->key()] = 1;
}

/**
* \brief Reads key releases and updates key bindings accordingly
*
*
*/
void ShalimonePlayer::keyReleaseEvent(QKeyEvent *event){
    KeyBindings[event->key()] = 0;
}

/**
* \brief Calls the methods to check for collision and then update the player's position
*
*
*/
void ShalimonePlayer::Update(){
    UpdateCollision();
    UpdateMovement();
}

/**
* \brief Checks for collision with both platforms and pickups
*
* Checks for collision with a platform to stand on it and move with it
* Check for collision with pickup to pick it up and have the score affected accordingly
*
*/
void ShalimonePlayer::UpdateCollision(){

   grounded = false;
   QList<QGraphicsItem* > list = collidingItems();
    for(auto &u : list) {
        if(ShalimonePlatform * platform = dynamic_cast<ShalimonePlatform *> (u)) {
            if(platform->y() > this->y()+this->pixmap().height()*0.85){
                if(dy > 0){
                    grounded = true;
                    dy=0;
                    platform_speed = platform->speed;
                }
                else if(dy == 0 && platform_speed == platform->speed)
                    grounded = true;
            }
        }
        else if(ShalimonePickup *pickup = dynamic_cast<ShalimonePickup*>(u)){
              if(pickup->x() )
                if(pickup->good)
                  this->current_score += PICKUP_SCORE;
              else
                  this->current_score = std::max(0, this->current_score - PICKUP_SCORE/2);
              this->scene()->removeItem(pickup);
              delete pickup;
        }
    }
    if(!grounded)
        platform_speed = 0;
}

/**
* \brief Updates the player's movement
*
* Checks if the player is either going right or left and/or jumping and updates position accordingly
* Takes care of the physics associated with jumping and falling from  platforms
* Also used to check if the player has fallen to the abyss and has lost
* Also updates the player's sprite
*
*/
void ShalimonePlayer::UpdateMovement(){
    dx = 0;
    if(KeyBindings[right] && this->x() < SHALIMONE_SCENE_WIDTH-this->pixmap().width()*0.8) {
        dx = horizontal_speed;
    }

    if(this->x()+this->pixmap().width()*0.2 > 0) {
        if(KeyBindings[left])
            dx = -horizontal_speed;
//        dx -= (platform_speed*MOVEMENT_TIMER_FREQ)/PLATFORM_TIMER_FREQ;
        dx -= platform_speed/2;
    }

    //LOSE
    if(this->y() >= SHALIMONE_SCENE_HEIGHT - this->height/2){
        this->movementTimer->stop();
        play_again_window->show();
        play_again_window->setHighScore(std::max(high_score, current_score));
        play_again_window->setScore(current_score);
        this->scene()->views().at(0)->hide();
        dy=0;

        if(this->current_score > this->high_score){
            std::ofstream outFile;
            outFile.open("highscore.txt");
            outFile<<this->current_score;
            outFile.close();
        }
    }

    //HANDLING JUMP PRESS
    if(KeyBindings[jump]){
        if(grounded){
            grounded = false;
            platform_speed = 0;
            dy = -jump_speed;
            jump_starting_position = this->y();
        }
    }

    if(!grounded) {
        //SPEED UPDATE
        //dy = dy + gravity;
        dy = std::min(dy + vertical_acceleration, jump_speed*0.7);
        //ACCELERATION UPDATE
        //no update - no forces involved yet. left for bouncing
        vertical_acceleration = gravity;
    }

    //POSITION UPDATE
    this->setPos(this->x() + dx, this->y() + dy);

    //SPRITE UPDATE
    UpdateSprite();
}

/**
* \brief Updates the player's sprite to animate the character using a state machine
*
*   State machine that updates the player's sprite and animation according tot he current state
*/
void ShalimonePlayer::UpdateSprite(){

    //SET SPRITE STATE
    if(grounded){
        this->sprite = this->idle_sprite;
    }else{
        if(dy > 0){ //falling
            this->sprite = this->falling_sprite;
        }else if(dy < 0){
            this->sprite = this->jumping_sprite;
        }else{
            this->sprite = this->idle_sprite;
        }
    }

    //ANIMATE SPRITE
    this->setPixmap(this->sprite->getPixmap());

}

/**
* \brief Initializes the sprites for each state used in the UpdateSprite() method
*
*/
void ShalimonePlayer::InitializeSprites(){

    this->falling_sprite = new Sprite(0,":/images/falling.png", 40, 40, PLAYER_SIZE, PLAYER_SIZE, 7);
    this->jumping_sprite = new Sprite(0,":/images/jumping.png", 40, 40, PLAYER_SIZE, PLAYER_SIZE, 7);
    this->idle_sprite = new Sprite(0,":/images/idle.png", 40, 40, PLAYER_SIZE, PLAYER_SIZE, 17);

}
