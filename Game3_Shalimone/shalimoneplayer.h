/**
* \file shalimoneplayer.h
* \brief ShalimonePlayer class
*
* Used to implement the player
*/

#ifndef SHALIMONEPLAYER_H
#define SHALIMONEPLAYER_H

#include <QObject>
#include <QWidget>
#include <QGraphicsRectItem>
#include <QGraphicsPixmapItem>
#include <QKeyEvent>
#include <QTimer>
#include <QFile>
#include <unordered_map>
#include <vector>
#include <math.h>
#include <fstream>
#include <QTextStream>
#include "shalimoneplatform.h"
#include "shalimonepickup.h"
#include "globalparameters.h"
#include "shalimoneplayagain.h"
#include "sprite.h"
#include <QGraphicsView>
#include <algorithm>


class ShalimonePlayer : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    explicit ShalimonePlayer(QObject *_parent = nullptr, QWidget* parent = nullptr, QWidget* _framework = nullptr); //!< ShalimonePlayer constructor
    void keyPressEvent(QKeyEvent *event); //!< Checks for key presses
    void keyReleaseEvent(QKeyEvent *event); //!< Checks for key releases
    std::unordered_map<int, bool> KeyBindings; //!< Holds the currently pressed keys
    void ResetKeys(); //!< Resets the key bindings to false

    int up = Qt::Key_Up; //!< Abbreviation for the up key
    int down = Qt::Key_Down; //!< Abbreviation for the down key
    int right = Qt::Key_Right; //!< Abbreviation for the right key
    int left = Qt::Key_Left; //!< Abbreviation for the left key
    int jump = Qt::Key_Space; //!< Abbreviation for the space key
    int current_score; //!< Current score held by the player
    int high_score; //!< Highest score ever reached by the player

    ShalimonePlayAgain* play_again_window;
    QWidget* parent;
    QWidget* framework;

    QGraphicsView * parent_view;

    qreal horizontal_speed; //!< Horizontal speed of the player
    qreal vertical_acceleration; //!<Vertical acceleration of the player
    qreal current_jump_height; //!< Current jump height
    bool grounded; //!< Grounding player for physics control
    qreal gravity; //!< Gravity applied to the player
    qreal acceleration; //!< Acceleration applied to the player

    qreal platform_speed; //!< Speed of the platform the player is currently standing on

    qreal dx, dy; //!< Position change related variables
    qreal height; //!< Define player sprite height
    qreal jump_speed; //!< Force that allows the jump height to be relative to the time the jump key is pressed

    qreal jump_starting_position; //!<Stores the initial y when a jump is initiated
    QTimer *movementTimer; //!< Timer that slots into updateMovement();

    Sprite *sprite; //!< Sprite definition
    Sprite *idle_sprite; //!< Sprite definition
    Sprite *falling_sprite; //!< Sprite definition
    Sprite *jumping_sprite; //!< Sprite definition

    void UpdateCollision(); //!< Checks for collisions with other objects;
    void UpdateMovement(); //!< Checks for currently pressed keys and update the player's position

    void InitializeSprites(); //!< Initializes sprites with necessary spritesheets and parameters
    void UpdateSprite(); //!< Updates sprite frames and sprite state (falling, jumping, idle)


signals:
    void resetScene();

public slots:
    void Update(); //!< Checks for collisions and updates the player's movement
    void newGame();
};

#endif // SHALIMONEPLAYER_H
