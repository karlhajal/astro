/**
* \file shalimonescene.h
* \brief ShalimoneScene class
*
* Used as a game manager to implement the game's view and mechanics
*/

#ifndef SHALIMONESCENE_H
#define SHALIMONESCENE_H

#include <QGraphicsItem>
#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <stdlib.h>
#include <time.h>
#include "shalimoneplayer.h"
#include "shalimoneplatform.h"
#include "shalimonepickup.h"
#include "globalparameters.h"
#include <random>

using namespace std;
class ShalimoneScene : public QGraphicsScene
{
Q_OBJECT
public:
    ShalimoneScene(QWidget *parent = nullptr, QWidget* _framework = nullptr); //!< ShalimoneScene constructor
    void FillValues(); //!< Fills the values vectors with values from text files


private:
    QWidget * parent;
    QWidget *framework;
    ShalimonePlayer* player; //!< ShalimonePlayer object controlled by the player, it is set as the focus
    long current_score; //!< Keeps track of the player's current score
    long high_score; //!< Stores the player's all time highscore
    QGraphicsTextItem* score_text; //!< Displays the player's current score
    QGraphicsTextItem* highscore_text; //!< Displays the player's all time highscore
    QTimer* platform_timer;
    QTimer* score_timer;
    std::vector<QString> goodValues; ///< a vector that holds the good values to be instantiated
    std::vector<QString> badValues;  ///< a vector that holds the bad values to be instantiated

    Sprite *background_sprite; //!< Sprite definition
    QTimer *background_timer;

    //ALGORITHM SPECIFIC VARS
    int oldY; //!< Saves oldest Y spawn position in order to make new position reachable from it
    int max_reachable_distance;

    //LEVEL VARIABLES
    int level = 0;
    int platform_count = 0;
    std::vector<QString> platform_images = {":/images/blue_platform.png",
                                            ":/images/purple_platform.png",
                                            ":/images/green_platform.png",
                                            ":/images/red_platform.png"};

    std::vector<double> level_speeds = { 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5,
                                         6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10  };

    std::vector<int> upgrade_score = {10000, 20000, 50000, 100000, 150000, 200000};

    int seperation_timer;

public slots:

    void InstantiatePlatform();
    void AddScore();
    void UpdateBackground(); //!< Animate background sprite

    void new_game();


};

#endif // SHALIMONESCENE_H
