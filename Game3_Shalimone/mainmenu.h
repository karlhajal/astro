/**
 * @file mainmenu.h
 * @brief The MainMenu class
 *
 * Simple UI window that displays the game's main menu
 */

#ifndef MAINMENU_H
#define MAINMENU_H

#include <QWidget>
#include "shalimonescene.h"
#include <QGraphicsView>
#include "infowindow.h"

namespace Ui {
class MainMenu;
}

class MainMenu : public QWidget
{
    Q_OBJECT

public:
    explicit MainMenu(QWidget *parent = 0, QWidget* _parentWindow = nullptr);
    ~MainMenu();

public slots:
    void updateBackground();

private slots:
    void on_pushButton_2_clicked();
    void on_pushButton_clicked();
    void on_pushButton_3_clicked();

private:

    Ui::MainMenu *ui;
    ShalimoneScene* scene;
    QGraphicsView* view;
    Sprite *shalim;
    QTimer *bgtimer;
    QWidget* parentWindow;
    InfoWindow* info;

    void init();
};

#endif // MAINMENU_H
