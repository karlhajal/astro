/**
* \file sprite.cpp
* \brief contains Sprite class method definitions
*
*/

#include "sprite.h"
#include "globalparameters.h"

Sprite::Sprite(QObject *parent, QString spritesheet_location, int width, int height, int scalewidth, int scaleheight, int frames) :
/**
* \brief Sprite constructor
*
*   Initializes sprite and connects timer to the nextFrame method
*/
    QObject(parent)
{
    currentFrame = 0;   // Sets the coordinates of the current frame of the sprite
    spriteImage = new QPixmap(spritesheet_location); // Load the sprite image QPixmap
    sprite_width = width;
    sprite_height = height;
    scale_width = scalewidth;
    scale_height = scaleheight;
    nbframes = frames;
    timer = new QTimer();   // Create a timer for sprite animation
    connect(timer, &QTimer::timeout, this, &Sprite::nextFrame);
    timer->start(animation_speed);   // Run the sprite on the signal generation with a frequency of 25 ms
}

/**
* \brief Gets the object's bounding Rectangle
*
*
*/
QRectF Sprite::boundingRect() const
{
    return QRectF(-20,-20,20,20);
}

/**
* \brief Gets the object's pixmap
*
*
*/
QPixmap Sprite::getPixmap()
{
    /* In the graphic renderer we draw the sprite
     * The first two arguments - is the X and Y coordinates of where to put QPixmap
     * The third argument - a pointer to QPixmap
     * 4 and 5 of the arguments - The coordinates in the image QPixmap, where the image is displayed
     * By setting the X coordinate with the variable currentFrame we would like to move the camera on the sprite
     * and the last two arguments - the width and height of the displayed area, that is, the frame
     * */
//    return QPixmap(-20, -20, *spriteImage, currentFrame, 0, 20, 20);
    return (spriteImage->copy(currentFrame, 0, sprite_width,sprite_height)).scaled(scale_width,scale_height);

}

/**
* \brief Slot for turning images into QPixmap
*
*
*/
void Sprite::nextFrame()
{
    /* At a signal from the timer 20 to move the point of rendering pixels
     * If currentFrame = 300 then zero out it as sprite sheet size of 300 pixels by 20
     * */
    currentFrame += sprite_width;
    if (currentFrame >= nbframes*sprite_width ) currentFrame = 0;
}
