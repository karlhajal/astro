/**
* \file valuepickup.h
* \brief ValuePickup class
*
* Used to implement the good and bad value pickups
*/

#ifndef VALUEPICKUP_H
#define VALUEPICKUP_H

#include <QObject>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QGraphicsPixmapItem>
#include <QPainter>
#include <QTimer>
#include <QThread>
#include "stdlib.h"
#include <QString>
#include <fstream>

class ValuePickup : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    int speed; //!< Movement speed + direction
    int x_lboundary; //!< Screen's left boundary
    int x_rboundary; //!< Screen's right boundary
    bool good; //!< Specifies whether the pickup is good or bad
    QString valuename; //!< Contains the name of the trait represented by the pickup

    explicit ValuePickup(QObject *parent = nullptr); //!< ValuePickup constructor
    void setImage(); //!< Sets the image associated with the pickup and adjusts it according to the direction of movement

signals:

public slots:

        void updatePosition(); //!< adds movement to the pickup
};

#endif // VALUEPICKUP_H
