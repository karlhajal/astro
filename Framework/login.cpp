/**
* \file login.cpp
* \brief contains login class method definitions
*
*/

#include "login.h"
#include "accountwindow.h"
#include "signUp.h"
#include <QtWidgets>


/**
* \brief login constructor
*
* Initializes all objects, connects buttons to slots and sets them up in a grid layout and then a vertical layout.
*
*/
login::login(QWidget *parent) : QWidget(parent)
{

    //INITIALIZATION
    title = new QLabel("Astro");
    title->setAlignment(Qt::AlignCenter);
    title->setMargin(title_margin);
    title->setStyleSheet("QLabel{ font-size:50px; height:100px }");
    username = new QLineEdit();
    username->setPlaceholderText("Username");
    password = new QLineEdit();
    password->setPlaceholderText("Password");
    password->setEchoMode(QLineEdit::Password);
    signUpWindow = new signUp(nullptr, this);
    signIn = new QPushButton("Sign In");
    signUpButton = new QPushButton("Sign Up");
    guest = new QPushButton("Continue as  guest");

    mainLayout = new QVBoxLayout;
    grid = new QGridLayout;

    connect(signIn, SIGNAL(clicked(bool)), this, SLOT(signInSlot()));
    connect(guest, SIGNAL(clicked(bool)), this, SLOT(guestSlot()));
    connect(signUpButton, SIGNAL(clicked(bool)), this, SLOT(signUpSlot()));

    setGridLayout();
    setVerticalLayout();
    setLayout(mainLayout);
    setFixedWidth(500);
}

/**
* \brief signIn slot
*
*
*/
void login::signInSlot() {
    accountWindow *acctWindow = new accountWindow(nullptr, this);
    acctWindow->show();
    this->hide();
}

/**
* \brief guest slot
*
* Sign in as guest
*/
void login::guestSlot(){
    accountWindow *acctWindow = new accountWindow(nullptr,this);
    acctWindow->show();
    this->hide();
}

/**
* \brief signUp slot
*
* Open the sign up window
*/
void login::signUpSlot() {
    signUpWindow->show();
    this->hide();
}

/**
* \brief Sets grid and objects in vertical layout
*
*/
void login::setVerticalLayout(){
    mainLayout->setAlignment(Qt::AlignCenter);
    mainLayout->addWidget(title);
    mainLayout->addItem(new QSpacerItem(10,50));
    mainLayout->addItem(grid);
    mainLayout->setMargin(mainMargin);
    mainLayout->setSpacing(0);
    mainLayout->setStretch(0,1);
}

/**
* \brief Sets objects in grid
*
*/
void login::setGridLayout(){
    grid->addWidget(username, 0,0,1,3);

    grid->addItem(new QSpacerItem(10,20), 1,0,1,3);

    grid->addWidget(password, 2,0,1,3);

    grid->addItem(new QSpacerItem(10,20), 3,0,1,3);

    grid->addWidget(signIn, 4,0);
    grid->addItem(new QSpacerItem(20,10),4,1);
    grid->addWidget(signUpButton, 4,2);

    grid->addItem(new QSpacerItem(10,10), 5,0,1,3);


    grid->addWidget(guest,6,0,1,3);
}
