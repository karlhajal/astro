/**
* \file bush.h
* \brief ValuePickup class
*
* Used to implement the good and bad value pickups
*/

#ifndef BUSH_H
#define BUSH_H

#include <QObject>
#include <QGraphicsPixmapItem>

class Bush : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    explicit Bush(QObject *parent = 0, bool tall = 0); //!< Bush constructor
signals:

public slots:
};

#endif // BUSH_H
