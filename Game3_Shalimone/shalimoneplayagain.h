/**
 * @file shalimoneplayagain.h
 * @brief The ShalimonePlayAgain class
 *
 * Simple UI window that shows the play again screen
 */

#ifndef SHALIMONEPLAYAGAIN_H
#define SHALIMONEPLAYAGAIN_H

#include <QDialog>
#include <QGraphicsView>
#include <QGraphicsScene>


namespace Ui {
class ShalimonePlayAgain;
}

class ShalimonePlayAgain : public QDialog
{
    Q_OBJECT

public:
    explicit ShalimonePlayAgain(QWidget *parent = 0, QWidget* _framework = nullptr ,int _high_score = 0, int _current_score = 0);
    ~ShalimonePlayAgain();
    QWidget* parent;
    int high_score, current_score;

    void setScore(int _current_score= 0);

    void setHighScore(int _high_score = 0);
private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

signals:
    void resetGame();

private:
    QGraphicsView * view;
    Ui::ShalimonePlayAgain *ui;
    QWidget* framework;

};

#endif // SHALIMONEPLAYAGAIN_H
