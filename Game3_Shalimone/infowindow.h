/**
 * @file info.h
 * @brief The Info class
 *
 * Simple UI window that shows information about the game
 */

#ifndef INFOWINDOW_H
#define INFOWINDOW_H

#include <QWidget>

namespace Ui {
class InfoWindow;
}

class InfoWindow : public QWidget
{
    Q_OBJECT

public:
    explicit InfoWindow(QWidget *parent = 0, QWidget *_parent = 0);
    QWidget* parent;
    ~InfoWindow();

private slots:
    void on_pushButton_clicked();

private:
    Ui::InfoWindow *ui;
};

#endif // INFOWINDOW_H
