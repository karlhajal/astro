#ifndef NODE_DETAILS_H
#define NODE_DETAILS_H

#include <QString>
#include <QVector>

using namespace std;

/**
 * @file node_details.h
 * @brief The NodeDetails class
 *
 * This class contains all the information needed to display
 * a certain situation in a scenario, we call our situations
 * "nodes" in order to relate it to a graph, since our story is really
 * a directed acyclic graph
 */
class NodeDetails{

public:
    QString button1Text; /**< The text that displays on the push button for choice 1 */
    QString button2Text; /**< The text that displays on the push button for choice 2 */

    QVector<int> valueVector1, valueVector2; /**< These vectors hold the change in values due to choosing choice 1 or 2 */

    NodeDetails *choice1; /**< A pointer to the node reachable by choice 1 */
    NodeDetails *choice2; /**< A pointer to the node reachable by choicd 2 */

    QString bg_image; /**< A string that hold the name of the background image associated with this node */

    QVector<QString> dialogue_text; /**< The dialog that appears before a choice */
    QStringList sound_files; /**< The names of the sound files to play for each dialog */
};





#endif // NODE_DETAILS_H
