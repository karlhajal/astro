/**
* \file shalimoneplatform.h
* \brief ShalimonePlatform class
*
* Used to implement the platforms
*/

#ifndef SHALIMONEPLATFORM_H
#define SHALIMONEPLATFORM_H

#include <QObject>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QGraphicsPixmapItem>
#include <QTimer>
#include "globalparameters.h"

class ShalimonePlatform : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    explicit ShalimonePlatform(QObject *parent = nullptr, int _speed = 20, QString image_path = ""); //!< ShalimonePlatform constructor
    int speed; //!< Movement speed of the platform

private:

signals:

public slots:
    void UpdatePosition(); //!< Updates the drink's position
};

#endif // SHALIMONEPLATFORM_H
