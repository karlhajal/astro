/**
* \file infowindow.cpp
* \brief contains InfoWindow class method definitions
*
*/

#include "infowindow.h"
#include "ui_infowindow.h"

InfoWindow::InfoWindow(QWidget *parent, QWidget *_parent) :
    QWidget(parent),
    ui(new Ui::InfoWindow)
{
    this->parent = _parent;
    ui->setupUi(this);
}

InfoWindow::~InfoWindow()
{
    delete ui;
}

void InfoWindow::on_pushButton_clicked()
{
    this->hide();
    parent->show();
}
