/**
 * @file storynode.cpp
 * @brief StoryNode class method definitions
 *
 */

#include "storynode.h"
#include "ui_storynode.h"
#include <QPixmap>

/**
* @brief StoryNode constructor
*
* Sets up the UI, instantiates the QMediaPlayer object, sets up the Timer and calls the reset() method
*
*/
StoryNode::StoryNode(NodeDetails *node_details, QWidget *parent, const QVector<int> & values, QString user, int scenario,
                     NodeDetails *first_node) :

    QDialog(parent),
    ui(new Ui::StoryNode)
{
    ui->setupUi(this);
    this->user = user;
    this->scenario = scenario;
    this->start_node = first_node;

    ui->happinessBar->setValue(values[0]);
    ui->adventureBar->setValue(values[1]);
    ui->acceptanceBar->setValue(values[2]);
    ui->persistenceBar->setValue(values[3]);

    main_menu = parent;
    this->setFixedSize(800, 600);
    node_list = node_details;
    media_player = new QMediaPlayer();

    time_per_decision = 5;
    time_left = time_per_decision;
    ui->timer->setText("Time: 0:0" + QString::number(time_per_decision));
    start_timer = false;
    update_timer = new QTimer(this);
    connect(update_timer, SIGNAL(timeout()), this, SLOT(Update()));
    update_timer->start(1000);
    reset();
}

/**
* @brief StoryNode destructor
*
* Deletes the UI
*
*/
StoryNode::~StoryNode() {
    delete ui;
}

/**
* @brief Resets the Progress Bars
*
* Reverts the value progress bars to their initial values
*
*/
void StoryNode::resetProgressBars() {
    ui->happinessBar->setValue(50);
    ui->persistenceBar->setValue(50);
    ui->acceptanceBar->setValue(50);
    ui->adventureBar->setValue(50);
}

/**
* @brief Resets a StoryNode object
*
* Sets the current NodeDetails node to the initial one, resets the progress bars and calls initialize_screen()
*
*/
void StoryNode::reset() {
    node = node_list[0];
    resetProgressBars();
    initialize_screen();
}

/**
* @brief Initializes the screen according to the current NodeDetails node's elements
*
* Resets the timer, sets a new image, shows the first line of dialogue, resets n_text and plays the first sound
*
*/
void StoryNode::initialize_screen() {

    std::ofstream outFile((user + "_2.txt").toStdString(), std::ios_base::trunc);
    std::string save_scenario = (scenario == 1) ? "1" : "2";
    int index = 25;
    for(int i = 0 ; i < 8; i++) {
        NodeDetails* first = &node_list[i];
        NodeDetails* second = start_node;
        if(first == second) index = i;
    }
    outFile << save_scenario << " " << std::to_string(index) << " " <<
               std::to_string(ui->happinessBar->value()) << " " << std::to_string(ui->adventureBar->value()) << " " <<
               std::to_string(ui->acceptanceBar->value()) << " " << std::to_string(ui->persistenceBar->value()) << '\n';

    start_timer = false;
    time_left = time_per_decision;
    ui->timer->setText("Time: 0:0" + QString::number(time_per_decision));
    update_timer->start();

    n_text = 0;

    ui->button1->setVisible(false);
    ui->button2->setVisible(false);
    ui->image_label->setPixmap(QPixmap(node.bg_image).scaledToHeight(450).scaledToWidth(600));

    ui->button1->setText(node.button1Text);
    ui->button2->setText(node.button2Text);
    ui->dialogue_label->setText(node.dialogue_text[n_text]);

    ui->dialogue_button->setVisible(true);
    ui->dialogue_label->setVisible(true);

    if(node.choice2 != NULL && node.choice1 != NULL) {
        media_player->stop();
        media_player->setMedia(QUrl("qrc" + node.sound_files[n_text++]));
        media_player->setVolume(100);
        media_player->play();
    }
}

/**
* @brief Initiates the transition to a new node when button 1 is clicked
*
* Called when the button related to choice 1 is clicked.
* Updates the progress bars' values according to the choice taken and intitiates the transition to the next step
* If we are dealing with an end node, end the game and show the stats screen
*
*/
void StoryNode::on_button1_clicked()
{ 
    if(node.choice1 == NULL) {

        ui->happinessBar->setValue(std::min(100, std::max(0, ui->happinessBar->value() + node.valueVector1[0])));
        ui->adventureBar->setValue(std::min(100, std::max(0, ui->adventureBar->value() + node.valueVector1[1])));
        ui->acceptanceBar->setValue(std::min(100, std::max(0, ui->acceptanceBar->value() + node.valueVector1[2])));
        ui->persistenceBar->setValue(std::min(100, std::max(0, ui->persistenceBar->value() + node.valueVector1[3])));

        play_again_screen = new PlayAgain(main_menu, this, ui->happinessBar->value(), ui->adventureBar->value(),
                                          ui->acceptanceBar->value(), ui->persistenceBar->value(),
                                          ui->decisivenessBar->value());
        reset();
        this->hide();
        play_again_screen->show();
        return;
    }

    ui->happinessBar->setValue(std::min(100, std::max(0, ui->happinessBar->value() + node.valueVector1[0])));
    ui->adventureBar->setValue(std::min(100, std::max(0, ui->adventureBar->value() + node.valueVector1[1])));
    ui->acceptanceBar->setValue(std::min(100, std::max(0, ui->acceptanceBar->value() + node.valueVector1[2])));
    ui->persistenceBar->setValue(std::min(100, std::max(0, ui->persistenceBar->value() + node.valueVector1[3])));

    node = *(node.choice1);
    start_node = node.choice1;
    initialize_screen();
}


/**
* @brief Initiates the transition to a new node when button 2 is clicked
*
* Called when the button related to choice 2 is clicked.
* Updates the progress bars' values according to the choice taken and intitiates the transition to the next step
* If we are dealing with an end node, end the game and show the stats screen
*
*/
void StoryNode::on_button2_clicked()
{
    if(node.choice2 == NULL) {
        ui->happinessBar->setValue(std::min(100, std::max(0, ui->happinessBar->value() + node.valueVector1[0])));
        ui->adventureBar->setValue(std::min(100, std::max(0, ui->adventureBar->value() + node.valueVector1[1])));
        ui->acceptanceBar->setValue(std::min(100, std::max(0, ui->acceptanceBar->value() + node.valueVector1[2])));
        ui->persistenceBar->setValue(std::min(100, std::max(0, ui->persistenceBar->value() + node.valueVector1[3])));

        play_again_screen = new PlayAgain(main_menu, this, ui->happinessBar->value(), ui->adventureBar->value(),
                                          ui->acceptanceBar->value(), ui->persistenceBar->value(),
                                          ui->decisivenessBar->value());
        reset();
        this->hide();
        play_again_screen->show();
        return;
    }

    ui->happinessBar->setValue(std::min(100, std::max(0, ui->happinessBar->value() + node.valueVector2[0])));
    ui->adventureBar->setValue(std::min(100, std::max(0, ui->adventureBar->value() + node.valueVector2[1])));
    ui->acceptanceBar->setValue(std::min(100, std::max(0, ui->acceptanceBar->value() + node.valueVector2[2])));
    ui->persistenceBar->setValue(std::min(100, std::max(0, ui->persistenceBar->value() + node.valueVector2[3])));

    node = *(node.choice2);
    start_node = node.choice2;

    initialize_screen();
}

/**
* @brief Switches to the next line of dialogue/decision making when the dialogue button is clicked
*
* Show the next line of dialogue if it exists and plays the corresponding sound
* If all lines of dialogue have been shown, the decision making step is initiated
*
*/
void StoryNode::on_dialogue_button_clicked()
{
    if( n_text < node.dialogue_text.size() ) {

        ui->dialogue_label->setText(node.dialogue_text[n_text]);
        media_player->stop();
        media_player->setMedia(QUrl("qrc" + node.sound_files[n_text++]));
        media_player->setVolume(100);
        media_player->play();

    } else {
        ui->button1->setVisible(true);
        ui->button2->setVisible(true);
        ui->dialogue_button->setVisible(false);
        ui->dialogue_label->setVisible(false);
        start_timer = true;
    }
}

/**
* @brief Updates the timer during the decision making step
*
* Called everytime our QTimer object times out
* Decrements the on screen timer on each timeout during the decision making step
* If time runs out, the player is given a penalty
*
*/
void StoryNode::Update(){
    if(!start_timer)
        return;
    if(time_left > 0){
        time_left--;
        ui->timer->setText("Time: 0:0" + QString::number(time_left));
    }
    else{
        ui->decisivenessBar->setValue(std::max(0, ui->decisivenessBar->value() - 20));
        ui->timer->setText("Time is up, -20 decisiveness");
        update_timer->stop();
        return;
    }
}
