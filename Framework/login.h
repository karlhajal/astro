/**
* \file login.h
* \brief login class
*
* Used to implement the user login experience
*/

#ifndef LOGIN_H
#define LOGIN_H

#include <QWidget>
#include <QtWidgets>
#include "signUp.h"
class login : public QWidget
{
    Q_OBJECT
public:
    explicit login(QWidget *parent = nullptr); //!< login constructor
    int title_margin = 20, title_bottom_margin = 0,
    mainMargin=50, button_margin = 20;

    QVBoxLayout *mainLayout;

    QLabel *title;
    signUp * signUpWindow;
    QLineEdit *username;
    QLineEdit *password;
    QPushButton *signIn;
    QPushButton *signUpButton;
    QPushButton *guest;
    QGridLayout *grid;

private:
    void setGridLayout();
    void setVerticalLayout();

signals:

public slots:
    void signInSlot();
    void signUpSlot();
    void guestSlot();
};

#endif // LOGIN_H
