/**
* \file bush.cpp
* \brief contains Bush class method definitions
*
*/

#include "bush.h"

/**
* \brief Bush constructor
*
* Sets the Bush object's image according to the value specified by the tall variable
*
*/
Bush::Bush(QObject *parent, bool tall) : QObject(parent)
{
    if(tall) {
        this->setPixmap(QPixmap(":/images/tall_bush.png").scaledToHeight(75).scaledToWidth(75));
    }
    else {
        this->setPixmap(QPixmap(":/images/short_bush.png").scaledToHeight(75).scaledToWidth(75));
    }
}
