/**
* \file globalparameters.h
* \brief Includes some global parameters that are used in various classes.
*
*/

#ifndef GLOBALPARAMETERS_H
#define GLOBALPARAMETERS_H


const int GAME_SCENE_WIDTH = 1200, GAME_SCENE_HEIGHT = 800, PADDING = 25;



#endif // GLOBALPARAMETERS_H
