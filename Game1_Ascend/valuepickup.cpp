/**
* \file valuepickup.cpp
* \brief contains ValuePickup class method definitions
*
*/

#include "valuepickup.h"
#include "globalparameters.h"

/**
* \brief ValuePickup constructor
*
* Sets the boundaries, creates a timer and connects it to the updatePosition() method.
*
*/
ValuePickup::ValuePickup(QObject *parent) : QObject(parent)
{

    QTimer* timer = new QTimer(this);
    this->x_lboundary = -10;
    this->x_rboundary = 1510;

    connect(timer, SIGNAL(timeout()),this,SLOT(updatePosition()));
    timer->start(10);
}

/**
* \brief Sets the pickup object's image
*
* Sets the pickup object's image, adjusts it according to movememnt direction and adds the represented Trait's text on top
*
*/
void ValuePickup::setImage(){
    QString str;
    if(this->good)
        str = ":/images/mage-blue.png";
    else
        str = ":/images/mage-black.png";
    QImage image(str);
    if(this->speed<0)
      image = image.mirrored(1,0);
    QPainter p(&image);
    p.setPen(QPen(Qt::white));
    p.setFont(QFont("Times", 50, QFont::Bold));
    p.drawText(image.rect(), Qt::AlignCenter, this->valuename.toUpper());
    p.end();
    this->setPixmap(QPixmap::fromImage(image).scaled(GAME_SCENE_HEIGHT/9,GAME_SCENE_HEIGHT/9));
}

/**
* \brief Adds movement to the pickups
*
* The pickups move in the direction specified by the speed variable's sign.
* Also each pickup is deleted upon getting out of the view.
*/
void ValuePickup::updatePosition(){
    this->setPos(this->x()+this->speed,this->y());
    if(this->x()>this->x_rboundary || this->x() < this->x_lboundary){
        scene()->removeItem(this);
        delete this;
    }
}


