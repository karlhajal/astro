/**
* \file loginform.cpp
* \brief LoginForm class method definition
*
*/

#include "loginform.h"
#include "ui_loginform.h"
#include "homepage.h"
#include "signUp.h"


/**
* @brief Constructor LoginForm
*
* Initializes all objects, connects buttons to slots and sets layout accordingly.
*
*/
LoginForm::LoginForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginForm)
{
    ui->setupUi(this);
}

/**
 * @brief Destructor LoginForm::~LoginForm
 */
LoginForm::~LoginForm()
{
    delete ui;
}

/**
 * @brief LoginForm::on_login_button_clicked
 * Checks login info with database, and decides whether or not to login user
 */
void LoginForm::on_login_button_clicked()
{
    QFile ifile("users.txt");
    if (!ifile.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    QString content = ifile.readAll();
    QStringList users = content.split('\n');
    users.removeLast();

    for(auto it = users.begin(); it != users.end(); it++) {
        QStringList current_user = it->split(":");
        QString username = current_user.at(0),
                password = current_user.at(1);
        QString input_username = ui->lineEdit->text(),
                input_password = ui->lineEdit_2->text();
        if(input_username == username && input_password == password) {
            Homepage *acctWindow = new Homepage(nullptr, this, username);
            acctWindow->show();
            this->hide();
            return;
        }
    }

    QMessageBox *bx = new QMessageBox();
    bx->setText("Invalid username or password");
    bx->exec();
}

/**
 * @brief LoginForm::on_guest_button_clicked
 * Signs in user as a guest - No need to check database - No data saved
 */
void LoginForm::on_guest_button_clicked()
{
    Homepage *acctWindow = new Homepage(nullptr,this);
    acctWindow->show();
    this->hide();
}

/**
 * @brief LoginForm::on_signup_button_clicked
 * Opens sign up window and hides login window.
 */
void LoginForm::on_signup_button_clicked()
{
    signUp *signUpWindow = new signUp(nullptr, this);
    signUpWindow->show();
    this->hide();
}
