/**
* \file shalimoneplatform.cpp
* \brief contains ShalimonePlatform class method definitions
*
*/

#include "shalimoneplatform.h"

/**
* \brief ShalimonePlatform constructor
*
* Sets the object's image, creates a timer and connects it to the UpdatePosition() method.
*
*/
ShalimonePlatform::ShalimonePlatform(QObject *parent, int _speed, QString image_path) : QObject(parent)
{
    this->setPixmap((QPixmap(image_path)).scaled(150,150));
    speed = _speed;
    QTimer* timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()),this,SLOT(UpdatePosition()));
    timer->start(PLATFORM_TIMER_FREQ);
}

/**
* \brief Adds movement to the platform
*
*/
void ShalimonePlatform::UpdatePosition(){

    this->setPos(this->x() - speed, this->y());
    if(this->x() < -this->pixmap().width()){
        scene()->removeItem(this);
        delete this;
    }
}
