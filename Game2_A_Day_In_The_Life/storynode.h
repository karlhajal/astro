/**
 * @file storynode.h
 * @brief The StoryNode class
 *
 * This class hold everything that is necessary for displaying a certain node in the scenario,
 * It displays everything that the current node pointer is pointing to in a nice user friendly way.
 * When the game end, we instantiate a screen with all the statistics and allow an option to play again.
 */

#ifndef STORYNODE_H
#define STORYNODE_H
#include <QFileInfo>
#include "node_details.h"
#include "playagain.h"
#include <QDialog>
#include <QMediaPlayer>
#include <fstream>
#include <QTimer>
#include <cmath>

namespace Ui {
class StoryNode;
}


class StoryNode : public QDialog
{
    Q_OBJECT

public:
    explicit StoryNode(NodeDetails *node_details, QWidget *parent = 0, const QVector<int>& values = QVector<int>(), QString user = "", int scenario = 1,
            NodeDetails* first_node = NULL); /**< StoryNode Constructor */
    ~StoryNode(); /**< StoryNode Destructor */
    QMediaPlayer *media_player; /**< QMediaPlayer object used to play sounds */
    NodeDetails node; /**< current node in the story that is being processed */
    NodeDetails *node_list; /**< A pointer to the array of nodes */
    QWidget *main_menu; /**< A pointer to the main menu widget */
    PlayAgain *play_again_screen; /**< A pointer to a PlayAgain object, which we will use to display data at the end of the game */
    int n_text; /**< An integer holding the current dialog String being shown */
    
    int time_left; /**< Integer that stores how much time is left to make a decision */
    bool start_timer; /**< Boolean that dictatest if the timer should be started */
    int time_per_decision; /**< Integer that stores how much time is allowed per decision */
    QTimer* update_timer; /**< QTimer object that is connected to the Update slot which updates the timer */
    
    int n_sound; /**< Integer that stores the number of the parsed sound file */
    QString user;
    int scenario;
    NodeDetails* start_node;

private slots:
    void reset(); /**< */

    void on_button1_clicked(); /**< Slot used to go to a certain path when button 1 is pressed */

    void on_button2_clicked(); /**< Slot used to go to a certain path when button 2 is pressed */

    void on_dialogue_button_clicked(); /**< Slot used to read through the dialogue text with the push of a button */

    void Update(); /**< Slot used to update the timer after a time period specified by update_timer */

private:
    Ui::StoryNode *ui; /**< A pointer to the forward declared class that Qt auto-generated from the ui file */
    void initialize_screen(); /**< Initializes the ui to show a new scene's background, dialogues and sound and resets the timer */
    void resetProgressBars(); /**< Resets the progress bars to their initial values */
};

#endif // STORYNODE_H
