/**
* \file shalimoneplayagain.cpp
* \brief ShalimonePlayAgain class method definition
*
*/


#include "shalimoneplayagain.h"
#include "ui_shalimoneplayagain.h"

ShalimonePlayAgain::ShalimonePlayAgain(QWidget *parent, QWidget* _framework,int _high_score, int _current_score) :
    QDialog(parent),
    ui(new Ui::ShalimonePlayAgain)
{
    this->parent = parent;
    framework = _framework;
    ui->setupUi(this);
}

void ShalimonePlayAgain::setScore(int _current_score) {
    ui->current_score_label->setText(QString::fromStdString("Score " + std::to_string(_current_score)));
}

void ShalimonePlayAgain::setHighScore(int _high_score) {
    ui->high_score_label->setText(QString::fromStdString("High Score " + std::to_string(_high_score)));
}

ShalimonePlayAgain::~ShalimonePlayAgain()
{
    delete ui;
}

void ShalimonePlayAgain::on_buttonBox_accepted()
{
   emit resetGame();
   this->hide();
   parent->show();
}

void ShalimonePlayAgain::on_buttonBox_rejected()
{
    this->hide();
    framework->show();
    this->close();
}
