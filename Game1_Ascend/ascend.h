/**
 * @file ascend.h
 * @brief The Ascend class
 *
 * Used as a game manager to implement the game's view and mechanics
 */

#ifndef ASCEND_H
#define ASCEND_H

#include <QObject>
#include <QGraphicsItem>
#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <QTimer>
#include <QFile>
#include <QWidget>
#include <QtWidgets>
#include <QKeyEvent>
#include "valuepickup.h"
#include "stdlib.h"
#include "bush.h"
#include <QMessageBox>
#include "player.h"
#include "globalparameters.h"
#include <QFont>

/// Main class for game1.
/// \author Rami Awar
/// \author Karl Hajal
/// \author Elie Maamary
///
class Ascend : public QWidget
{
Q_OBJECT
public:
    Player* player; ///< player member variable
    std::vector<QString> goodValues; ///< a vector that holds the good values to be instantiated
    std::vector<QString> badValues;  ///< a vector that holds the bad values to be instantiated
    int wins; ///< holds the times the player won so far
    int pickup_speed; ///< represents the speed at which the pickups move

	///Constructor for the class.
    explicit Ascend(QWidget *parent = nullptr, QWidget* _parentWindow = nullptr);

	///A function that displays the main menu before the game.
    void DisplayMainMenu();

	///fills the goodvalues and badvalues vectors.
    void fillValues();

	///clears the layout.
    void clearLayout(QLayout* layout);

	///returns the game to it's original state upon losing.
    void resetGame();


private:
    QPushButton* playButton; ///< used in the main menu
    QPushButton* quitButton; ///< used in the main menu

    QVBoxLayout* VertLayout; ///< used in the main menu and to display the gamescene

    QGraphicsScene *gameScene; ///< scene where everything takes place

	
    QGraphicsTextItem* Health, *Wins, *Time; ///< Used to display plays stats
    QGraphicsView *gameView; ///< View where the gamescene resides
    QWidget* parentWindow; 
    QTimer * updateTimer; ///< Checks for win/loss and updates the health and wins fields
    QTimer * timer; ///< instatiates the pickups
    int time_left, time_count; ///< holds the current time and time left


public slots:
	///initialize a new game.
    void newGame();

	///check for win/loss and update the health and wins fields.
    void update();

	///takes us back to the account window.
    void BackToAccountWindow();

	///instantiates a pickup at a random lane.
    void instantiatePickup();
};

#endif // ASCEND_H
