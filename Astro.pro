QT += core gui multimedia
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
SOURCES += \
    main.cpp \
    Framework/signUp.cpp \
    Game1_Ascend/ascend.cpp \
    Game1_Ascend/valuepickup.cpp\
    Game1_Ascend/player.cpp\
    Game1_Ascend/bush.cpp \
    Game2_A_Day_In_The_Life/mainwindow.cpp \
    Game2_A_Day_In_The_Life/storynode.cpp \
    Game2_A_Day_In_The_Life/playagain.cpp \
    Game2_A_Day_In_The_Life/info.cpp \
    Framework/loginform.cpp \
    Game3_Shalimone/shalimonescene.cpp \
    Game3_Shalimone/shalimoneplayer.cpp \
    Game3_Shalimone/sprite.cpp \
    Game3_Shalimone/shalimoneplatform.cpp \
    Game3_Shalimone/shalimonepickup.cpp \
    Game3_Shalimone/mainmenu.cpp \
    Game3_Shalimone/infowindow.cpp \
    Framework/homepage.cpp \
    Game3_Shalimone/shalimoneplayagain.cpp

HEADERS += \
    Framework/signUp.h \
    Game1_Ascend/ascend.h \
    Game1_Ascend/valuepickup.h \
    Game1_Ascend/player.h \
    Game1_Ascend/bush.h \
    Game1_Ascend/globalparameters.h \
    Game2_A_Day_In_The_Life/mainwindow.h\
    Game2_A_Day_In_The_Life/storynode.h\
    Game2_A_Day_In_The_Life/playagain.h\
    Game2_A_Day_In_The_Life/info.h\
    Game3_Shalimone/shalimonescene.h \
    Game3_Shalimone/globalparameters.h \
    Game3_Shalimone/shalimoneplayer.h \
    Game3_Shalimone/sprite.h \
    Game3_Shalimone/shalimoneplatform.h \
    Game3_Shalimone/shalimonepickup.h \
    Game3_Shalimone/mainmenu.h \
    Game3_Shalimone/infowindow.h\
    Framework/loginform.h \
    Framework/homepage.h \
    Game3_Shalimone/shalimoneplayagain.h


DISTFILES += \
    Framework/login.qss \
    images/orang.jpg \
    images/mememan.png \
    goodvalues.txt \
    badvalues.txt \
    whale.png \
    images/whale.png

RESOURCES += \
    resources.qrc\
    accounts.qrc \
    scenarios.qrc \
    sounds.qrc \
    stylesheets.qrc \
    images.qrc

FORMS += \
    Framework/loginform.ui \
    Framework/homepage.ui \
    Game2_A_Day_In_The_Life/mainwindow.ui \
    Game2_A_Day_In_The_Life/storynode.ui \
    Game2_A_Day_In_The_Life/playagain.ui \
    Game2_A_Day_In_The_Life/info.ui\
    Game3_Shalimone/mainmenu.ui\
    Game3_Shalimone/infowindow.ui \
    Game3_Shalimone/shalimoneplayagain.ui


