/**
* \file homepage.h
* \brief Homepage class
*
* Used to implement the framework's homepage that shows the 3 games and info about the user
*/


#ifndef HOMEPAGE_H
#define HOMEPAGE_H

#include <QDialog>

namespace Ui {
class Homepage;
}

class Homepage : public QDialog
{
    Q_OBJECT

public:
    explicit Homepage(QWidget *parent = 0, QWidget* _parentWindow=nullptr, QString user = "");
    ~Homepage();

private slots:
    void on_logout_button_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::Homepage *ui;
    QWidget *parentWindow;
    QString user;
};

#endif // HOMEPAGE_H
