/**
 * @file info.h
 * @brief The Info class
 *
 * Simple UI window that shows information about the game
 */

#ifndef INFO_H
#define INFO_H

#include <QMainWindow>
#include <QWidget>

namespace Ui {
class Info;
}

class Info : public QMainWindow
{
    Q_OBJECT

public:
    explicit Info(QWidget *parent = 0);
    ~Info();

private slots:
    void on_pushButton_clicked();

private:
    Ui::Info *ui;
    QWidget * parent_widget;
};

#endif // INFO_H
