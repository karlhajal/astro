Project for our Software Tools Lab course, the goal was to create a game distribution platform where a user can sign up, sign in, and play any of the three games we created, the theme of the project was "Human Values". The games were the following:

Game 1, Ascend: an action arcade game, the goal is to reach the end of the level using the directional arrow keys whilst collecting "good" pickups and dodging the "bad" pickups, which are denoted by positive and negative human values respectively.

Game 2, A day in the life: A story driven game where the player is presented with a scenario in the life of a certain individual, the player then has to make choices during the game which will affect the path that the story will take, the choices the player makes will be used to gauge certain values, represented as progress bars on the top of the screen

Game 3, Shalimone: A very simple action platformer in which the player has to basically jump on platforms using the directional arrow keys and the space bar.

A big problem we faced in the third game was randomly generating platforms is such a way that they were reachable by the player. We acheived this using a perlin noise algorithm https://en.wikipedia.org/wiki/Perlin_noise. This assured that the player could always reach the next platform.

Notable tools used during this project: Qt, C++, git (obviously), doxygen, GNU Make.