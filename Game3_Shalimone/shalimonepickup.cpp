/**
* \file shalimonepickup.cpp
* \brief contains ShalimonePickup class method definitions
*
*/

#include "shalimonepickup.h"

/**
* \brief ShalimonePickup constructor
*
* Sets the platform's speed, whether it's a good or bad value, and the value's name
* Also sets the art for the pickup
* Instantiates a QTimer and connects it to the UpdatePosition Slot
*
*/
ShalimonePickup::ShalimonePickup(QObject *parent, int _speed, QString _valuename, bool _good) : QObject(parent)
{
    speed = _speed;
    valuename = _valuename;
    good = _good;

    QImage image(":/images/candy.png");
    QPainter p(&image);
    p.setPen(QPen(Qt::black));
    p.setFont(QFont("Times", 15, QFont::Bold));
    p.drawText(image.rect(), Qt::AlignCenter, this->valuename.toUpper());
    p.end();
    this->setPixmap(QPixmap::fromImage(image));

    QTimer* timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()),this,SLOT(UpdatePosition()));
    timer->start(PLATFORM_TIMER_FREQ);
}

/**
* \brief Adds movement to the pickups
*
* Also each pickup is deleted upon getting out of the view if not collected.
*/
void ShalimonePickup::UpdatePosition(){
    this->setPos(this->x() - speed, this->y());
    if(this->x() < -this->pixmap().width()){
        scene()->removeItem(this);
        delete this;
    }
}
