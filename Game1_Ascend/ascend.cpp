/**
* \file ascend.cpp
* \brief Ascend class method definition
*
*/

#include "ascend.h"

/**
 * @brief      Constructs the Ascend game window.
 * 
 * All sizes are dependent on variables GAME_SCENE_HEIGHT and GAME_SCENE_WIDTH defined in global_parameters.h which is included in ascend.h
 * This allows for quick scaling of all objects on the screen by changing two variables.
 * 
 * @param      parent         The parent QWidget
 * @param      _parentWindow  The parent window, needed to switch back and forth from code.
 */
Ascend::Ascend(QWidget *parent, QWidget* _parentWindow)
{
    srand(time(NULL));
    setFixedSize(GAME_SCENE_WIDTH + PADDING, GAME_SCENE_HEIGHT + PADDING);//!< Size depends on variables GAME_SCENE_WIDTH and GAME_SCENE_HEIGHT and PADDING, defined in global_parameters.h which is included in ascend.h
    parentWindow = _parentWindow;
    wins = 0;
    VertLayout = new QVBoxLayout;
    DisplayMainMenu(); 
    setLayout(VertLayout);
}


/**
 * @brief      Creates the game main menu
 */


void Ascend::DisplayMainMenu(){
    QGraphicsTextItem* titleText = new QGraphicsTextItem("Ascend");
    QFont titleFont("arial", 50);
    titleText->setFont( titleFont);
    QGraphicsScene* backg = new QGraphicsScene(this);
    backg->addItem(titleText);
    QGraphicsView* backgView = new QGraphicsView;
    backgView->setFixedSize(GAME_SCENE_WIDTH + PADDING, GAME_SCENE_HEIGHT);
    backgView->setBackgroundBrush(QBrush(QImage(":/images/background_02.png").scaledToHeight(GAME_SCENE_HEIGHT).scaledToWidth(GAME_SCENE_WIDTH)));
    backgView->setScene(backg);

    QPushButton * playButton = new QPushButton("Play");
    QPushButton * quitButton = new QPushButton("Quit Game");

    connect(playButton, SIGNAL(clicked(bool)), this, SLOT(newGame()));
    connect(quitButton, SIGNAL(clicked(bool)), this, SLOT(BackToAccountWindow()));

    clearLayout(VertLayout);
    QGridLayout* grid = new QGridLayout;
    grid->addWidget(playButton,0,0);
    grid->addWidget(quitButton,0,1);
    //VertLayout->addWidget(playButton);
    //VertLayout->addWidget(quitButton);
    VertLayout->addWidget(backgView);
    VertLayout->addItem(grid);
}

/**
 * @brief      Function that hides current window and shows parent window.
 */
void Ascend::BackToAccountWindow(){
    this->hide();
    parentWindow->show();
    this->close();
}

/**
 * @brief      Check game status - Report status to player
 */
void Ascend::update() {

    if(player->satisfaction <= 0 || time_left <= 0) {
        QMessageBox msgBox;
        msgBox.setText("You lost! Try again");
        msgBox.exec();
        player->setPos(GAME_SCENE_WIDTH/2, 0);
        player->satisfaction = 5;
        player->resetKeys();
        player->goodValues.clear();
        player->badValues.clear();
        Time->setPlainText("Time: 1:00");
        time_left = 60;
    }

    if(player->y() >= 7*GAME_SCENE_HEIGHT/8) {
        QString GoodTraits = "Good Traits: \n";
        QString BadTraits = "\n Bad Traits: \n";
        for(auto it = player->goodValues.begin(); it!=player->goodValues.end(); it++)
            GoodTraits+= (*it + ", ");
        GoodTraits.chop(2);
        for(auto it = player->badValues.begin(); it!=player->badValues.end(); it++)
            BadTraits+= (*it + ", ");
        BadTraits.chop(2);

        QMessageBox msgBox;
        msgBox.setText("You won! Keep going!\n \n" + GoodTraits + '\n \n' +BadTraits);
        msgBox.exec();
        wins++;
        resetGame();
    }

    if(time_count<10)
        time_count++;
    else{
        time_count = 0;
        time_left--;
        if(time_left<10)
            Time->setPlainText("Time: 0:0" + QString::number(time_left));
        else
            Time->setPlainText("Time: 0:" + QString::number(time_left));
    }

    Health->setPlainText("Health: " + QString::number(player->satisfaction));
    Wins->setPlainText("Wins: " + QString::number(wins));

}

/**
 * @brief      Resets the game
 */
void Ascend::resetGame() {
    if(wins%2 == 0)
        gameScene->setBackgroundBrush(QBrush(QImage(":/images/background.png").scaledToHeight(GAME_SCENE_HEIGHT).scaledToWidth(GAME_SCENE_WIDTH)));
    else
        gameScene->setBackgroundBrush(QBrush(QImage(":/images/background_02.png").scaledToHeight(GAME_SCENE_HEIGHT).scaledToWidth(GAME_SCENE_WIDTH)));

    Time->setPlainText("Time: 1:00");
    player->setPos(GAME_SCENE_WIDTH/2, 0);
    player->satisfaction = 5;
    player->goodValues.clear();
    player->badValues.clear();
    this->time_count = 0;
    this->time_left = 60;
    player->resetKeys();
    pickup_speed = std::min(1+wins/2+1, 12); //Pickup speed will change proportionally to the number of wins
    timer->setInterval(std::max(50, 500-wins*25)); //frequence will increase proportionally to number of wins
}

/**
 * @brief      Creates a new game window.
 */
void Ascend::newGame(){
    gameScene = new QGraphicsScene(this);
    gameScene->setSceneRect(0,0,GAME_SCENE_WIDTH,GAME_SCENE_HEIGHT);
    gameView = new QGraphicsView;
    gameView->setScene(gameScene);
    clearLayout(VertLayout);
    VertLayout->addWidget(gameView);
    this->time_left = 60;
    this->fillValues();
    if(wins%2 == 0)
        gameScene->setBackgroundBrush(QBrush(QImage(":/images/background.png").scaledToHeight(GAME_SCENE_HEIGHT).scaledToWidth(GAME_SCENE_WIDTH)));
    else
        gameScene->setBackgroundBrush(QBrush(QImage(":/images/background_02.png").scaledToHeight(GAME_SCENE_HEIGHT).scaledToWidth(GAME_SCENE_WIDTH)));

    player = new Player();
    gameScene->addItem(player);
    player->setPos(GAME_SCENE_WIDTH/2, 0);
    player->setFlag(QGraphicsItem::ItemIsFocusable);
    player->setFocus();

    for(int i = 0; i < 5; i++) {
        Bush * bush = new Bush(NULL, rand()%2);
        int bush_x = rand()%GAME_SCENE_WIDTH;
        int bush_y = rand()%(GAME_SCENE_HEIGHT - GAME_SCENE_HEIGHT/8) + GAME_SCENE_HEIGHT/8;
        gameScene->addItem(bush);
        bush->setPos(bush_x, bush_y);
    }

    pickup_speed = 1+wins/2+1; //Pickup speed will change proportionally to the number of wins
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()),this,SLOT(instantiatePickup()));
    timer->start(500);

    updateTimer = new QTimer();
    connect(updateTimer, SIGNAL(timeout()), this, SLOT(update()));
    updateTimer->start(100);

    Time = new QGraphicsTextItem("Time: 1:00");
    Health = new QGraphicsTextItem("Health: " + QString::number(player->satisfaction));
    Wins = new QGraphicsTextItem("Wins: " + QString::number(wins));
    QFont serifFont("Times", 20);
    Health->setFont(serifFont);
    gameScene->addItem(Health);
    Health->setPos(0, 0);
    Wins->setFont(serifFont);
    gameScene->addItem(Wins);
    Wins->setPos(GAME_SCENE_WIDTH - 100, 0);
    Time->setFont(serifFont);
    gameScene->addItem(Time);
    Time->setPos(GAME_SCENE_WIDTH/2 - 50,0);

    if(!wins) {
        QMessageBox msgBox;
        msgBox.setText("Hello! Welcome to Ascend");
        msgBox.exec();
        msgBox.setText("You can move using the arrow keys");
        msgBox.exec();
        msgBox.setText("Don't hit the bushes, they knock you back");
        msgBox.exec();
        msgBox.setText("Reach the bottom of the level to win");
        msgBox.exec();
        msgBox.setText("Hitting blue mages gives you health for a maximum of 5, black mages reduce your health");
        msgBox.exec();
        msgBox.setText("If your health gets to 0, you'll have to start from the beginning");
        msgBox.exec();
        time_left = 60;
        time_count = 0;
    }

}


/**
 * @brief      Instantiates the moving text pickups and sets their initial position and speed and direction.
 */
void Ascend::instantiatePickup(){
    ValuePickup* newPickup = new ValuePickup();

    int random_num = rand()%1000;
    if(random_num < 750){
        newPickup->good = 0;
        newPickup->valuename = badValues[rand()%badValues.size()];
    }
    else{
        newPickup->good = 1;
        newPickup->valuename = goodValues[rand()%goodValues.size()];
    }


    gameScene->addItem(newPickup);
    random_num = rand()%6;
    if(random_num == 0){
        newPickup->speed=pickup_speed;
        newPickup->setPos(0, GAME_SCENE_HEIGHT/8);
    }
    else if(random_num == 1){
        newPickup->speed = -pickup_speed-1;
        newPickup->setPos(GAME_SCENE_WIDTH, 2*GAME_SCENE_HEIGHT/8);
    }
    else if(random_num == 2){
        newPickup->speed = +pickup_speed+1;
        newPickup->setPos(0,3*GAME_SCENE_HEIGHT/8);
    }
    else if(random_num == 3){
        newPickup->speed = -pickup_speed;
        newPickup->setPos(GAME_SCENE_WIDTH,4*GAME_SCENE_HEIGHT/8);
    }
    else if(random_num == 4){
        newPickup->speed = +pickup_speed;
        newPickup->setPos(0,5*GAME_SCENE_HEIGHT/8);
    }
    else if(random_num == 5){
        newPickup->speed = -pickup_speed-1;
        newPickup->setPos(GAME_SCENE_WIDTH, 6*GAME_SCENE_HEIGHT/8);
    }

    newPickup->setImage();
}

/**
 * @brief      Fills the pickup object text values from .txt files.
 */
void Ascend::fillValues(){
    while(this->goodValues.size()>0)
        this->goodValues.pop_back();
    while(this->badValues.size()>0)
        this->badValues.pop_back();

    QFile ifile(":/goodvalues.txt");
    if (!ifile.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    // read whole content
    QString content = ifile.readAll();
    // extract words
    QStringList list = content.split('\n');
    list.pop_back();
    for(auto it = list.begin(); it != list.end(); it++)
        this->goodValues.emplace_back(*it);
    ifile.close();

    QFile ifile2(":/badvalues.txt");
    if (!ifile2.open(QIODevice::ReadOnly | QIODevice::Text))
        return;
    content = ifile2.readAll();
    list = content.split('\n');
    list.pop_back();
    for(auto it = list.begin(); it != list.end(); it++)
        this->badValues.emplace_back(*it);
    ifile2.close();
}

/**
 * @brief      Clears layout from all objects.
 *
 * @param      layout  The main layout
 */
void Ascend::clearLayout(QLayout* layout)
{
    QLayoutItem* child;
    while(layout->count()!=0)
    {
        child = layout->takeAt(0);
        if(child->layout() != 0)
        {
            clearLayout(child->layout());
        }
        else if(child->widget() != 0)
        {
            delete child->widget();
        }

        delete child;
    }
}
