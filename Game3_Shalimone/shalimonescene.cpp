/**
* \file shalimonescene.cpp
* \brief contains shalimonescene class method definitions
*
*/

#include "shalimonescene.h"
#include <iostream>

//RANDOMNESS
unsigned long M = 4294967296,

    // a - 1 should be divisible by m's prime factors
    A = 1664525,

    // c and m should be co-prime
    C = 1;

    unsigned long Z = floor(rand()*M);

double generator(){
    Z = (A * Z + C) % M;
    return static_cast<double>(Z) / M;
}

void ShalimoneScene::UpdateBackground(){
    QPixmap t = background_sprite->getPixmap();

    this->setBackgroundBrush(QBrush(background_sprite->getPixmap()));
}

/**
* \brief ShalimoneScene constructor
*   Sets the scene and the background
*   Fills the good and bad values vectors fromt ext files
*   Initializes the player object and adds it to the scene
*   Initializes the score texts and places them on the scene
*   Initializes timer for platform spawning and connects it to InstantatePlatform slot
*   Initializes timer for score counting and connects it to AddScore slot
*   Instantiates initial platforms and sets player to initial position
*
*/
ShalimoneScene::ShalimoneScene(QWidget *parent, QWidget* _framework)
{
    background_sprite = new Sprite(0, ":/images/background.png", 30, 15, 1200, 800, 8);
    this->setBackgroundBrush(QBrush(QColor(200, 240, 255)));
    framework = _framework;
    //this->addPixmap(background_sprite->getPixmap());

    this->parent = parent;
    this->setSceneRect(0,0,SHALIMONE_SCENE_WIDTH,SHALIMONE_SCENE_HEIGHT);
//    this->setBackgroundBrush(QBrush(QImage(":/images/sky_background.jpg").scaledToHeight(SHALIMONE_SCENE_HEIGHT).scaledToWidth(SHALIMONE_SCENE_WIDTH)));

    //ALGORITHM RELATED
    oldY = 400;

    FillValues();

    srand(time(0));
    seperation_timer = PLATFORM_SPAWN_FREQ;
    player = new ShalimonePlayer(nullptr, parent, framework);
    connect(player, SIGNAL(resetScene()), this, SLOT(new_game()));
    this->addItem(player);
    player->setPos(SHALIMONE_SCENE_WIDTH/8 + 100, SHALIMONE_SCENE_HEIGHT*4/5 + 50 - player->pixmap().height()*1.1);
    player->setFlag(QGraphicsItem::ItemIsFocusable);
    player->setFocus();

    score_text = new QGraphicsTextItem("Score: 0");
    highscore_text = new QGraphicsTextItem("Highscore: " + QString::number(player->high_score));
    QFont serifFont("sans-serif", 20);
    score_text->setFont(serifFont);
    highscore_text->setFont(serifFont);
    this->addItem(score_text);
    this->addItem(highscore_text);
    score_text->setPos(20,15);
    highscore_text->setPos(SHALIMONE_SCENE_WIDTH*0.8,15);

    platform_timer = new QTimer(this);
    connect(platform_timer, SIGNAL(timeout()), this, SLOT(InstantiatePlatform()));
    platform_timer->start(PLATFORM_SPAWN_FREQ);

    score_timer = new QTimer(this);
    connect(score_timer, SIGNAL(timeout()), this, SLOT(AddScore()));
    score_timer->start(5);

    ShalimonePlatform *platform1 = new ShalimonePlatform(nullptr, PLATFORM_TIMER_FREQ*1/5, ":/images/blue_platform.png"),
                      *platform2 = new ShalimonePlatform(nullptr, PLATFORM_TIMER_FREQ*1/5, ":/images/blue_platform.png"),
                      *platform3 = new ShalimonePlatform(nullptr, PLATFORM_TIMER_FREQ*1/5, ":/images/blue_platform.png"),
                      *platform4 = new ShalimonePlatform(nullptr, PLATFORM_TIMER_FREQ*1/5, ":/images/blue_platform.png");
    this->addItem(platform1);
    this->addItem(platform2);
    this->addItem(platform3);
    this->addItem(platform4);

    platform1->setPos(SHALIMONE_SCENE_WIDTH/8 + 100, SHALIMONE_SCENE_HEIGHT*4/5 + 50);
    platform2->setPos(SHALIMONE_SCENE_WIDTH*3/8 + 100, SHALIMONE_SCENE_HEIGHT*3/5 + 50);
    platform3->setPos(SHALIMONE_SCENE_WIDTH*5/8 + 100, SHALIMONE_SCENE_HEIGHT*2/5 + 50);
    platform4->setPos(SHALIMONE_SCENE_WIDTH*7/8 + 100, SHALIMONE_SCENE_HEIGHT*1/5 + 50);

    this->setFocusItem(player);
}

void ShalimoneScene::new_game() {
    score_timer->stop();
    platform_timer->stop();
    player->movementTimer->stop();
}

/**
* \brief Instantiates platforms
*   Platform are instantiated according to a distribution
*   Each time a certain score is reached parameters are changed to make the game more challenging
*   New kinds of platforms are instantiated
*
*/
void ShalimoneScene::InstantiatePlatform(){

    if(player->current_score/upgrade_score[level] != 0) {
        level++;
        seperation_timer -=200;
        platform_timer->setInterval(seperation_timer);
    }

    int new_platform_speed = level_speeds[level]*PLATFORM_TIMER_FREQ*1/5;//Increase level difficulty every 10,000 score

    //int nb = rand()%3+1;
    //for (int i = 0; i < nb; i++){
    ShalimonePlatform* newPlatform = new ShalimonePlatform(nullptr, new_platform_speed, platform_images[level%4]);

    this->addItem(newPlatform);

    //CHECK INITIALIZATION UP THERE ^^^^
    //-------SPAWNING ALGORITHM---------

    int newY = 0;

    if(generator() < 0.3){
        newY = oldY + PLATFORM_SEPERATION;
    }else{
        newY = oldY - PLATFORM_SEPERATION;
    }

    if(newY >= PLATFORM_MAX_HEIGHT) newY = PLATFORM_MAX_HEIGHT;
    if(newY <= PLATFORM_MIN_HEIGHT) newY = PLATFORM_MIN_HEIGHT;

    if(newY == oldY){
        if(newY == PLATFORM_MAX_HEIGHT) newY = PLATFORM_MAX_HEIGHT - PLATFORM_SEPERATION;
        else if(newY == PLATFORM_MIN_HEIGHT) newY = PLATFORM_MIN_HEIGHT + PLATFORM_SEPERATION;
        else newY = oldY - PLATFORM_SEPERATION;
    }

    cout<<newY<<endl;
    newPlatform->setPos(SHALIMONE_SCENE_WIDTH+10, newY);//this is to get predefined locations seperated by multiples of 100

    oldY = newY;

    //----ADDING PICKUPS RANDOMLY ON TOP OF PLATFORMS-----//
    ShalimonePickup* newPickup;
    int temp = rand()%100;
    if(temp > 60){
        bool good;
        QString valuename;
        temp = rand()%100;
        if(temp < 50){
            good = true;
            valuename = goodValues[rand()%goodValues.size()];
        }else{
            good = false;
            valuename = badValues[rand()%badValues.size()];
        }

        newPickup = new ShalimonePickup(nullptr, new_platform_speed, valuename, good);
        
        newPickup->setPos(SHALIMONE_SCENE_WIDTH+10, newY - newPlatform->pixmap().height());
        this->addItem(newPickup);
    }

    player->current_score += 1;
    score_text->setPlainText("Score: " + QString::number(current_score));

}

/**
* \brief Adds the score to the player and checks if the highscore has been broken
*
*
*/
void ShalimoneScene::AddScore(){
    player->current_score += 1;
    score_text->setPlainText("Score: " + QString::number(player->current_score));
    if(player->current_score > player->high_score){
        highscore_text->setPlainText("Highscore: " + QString::number(player->current_score));
    }
}


/**
 * \brief      Fills the pickup object text values from .txt files.
 */
void ShalimoneScene::FillValues(){

    while(this->goodValues.size()>0)
        this->goodValues.pop_back();
    while(this->badValues.size()>0)
        this->badValues.pop_back();

    QFile ifile(":/goodvalues.txt");
    if (!ifile.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    // read whole content
    QString content = ifile.readAll();
    // extract words
    QStringList list = content.split('\n');
    list.pop_back();
    for(auto it = list.begin(); it != list.end(); it++)
        this->goodValues.emplace_back(*it);
    ifile.close();

    QFile ifile2(":/badvalues.txt");
    if (!ifile2.open(QIODevice::ReadOnly | QIODevice::Text))
        return;
    content = ifile2.readAll();
    list = content.split('\n');
    list.pop_back();
    for(auto it = list.begin(); it != list.end(); it++)
        this->badValues.emplace_back(*it);
    ifile2.close();
}
