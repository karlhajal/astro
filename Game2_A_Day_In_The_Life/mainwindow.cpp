/**
 * @file mainwindow.cpp
 * @brief MainWindow class method definitions
 *
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <fstream>
#include "info.h"
#include <sstream>
#include <iostream>
#include <QDebug>
#include <QFile>
#include <QTextStream>

using namespace std;

/**
* @brief MainWindow constructor
*
* Sets up the UI and sets the images
*
*/
 MainWindow::MainWindow(QWidget *parent, QWidget* _parentWindow, QString state, QString user) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    this->user = user;
     parentWindow = _parentWindow;
    ui->setupUi(this);
    ui->doctor_image->setPixmap(QPixmap(":/images/doctor.png").scaledToHeight(350).scaledToWidth(350));
    ui->teacher_image->setPixmap(QPixmap(":/images/teacher.png").scaledToHeight(351).scaledToWidth(350));

    QStringList items = state.split(" ");
    if(items.size() > 0) {
    QString scenario = items.at(0);
    QString offset = items.at(1);
    QVector<int> values;
    values.push_back(items.at(2).toInt());
    values.push_back(items.at(3).toInt());
    values.push_back(items.at(4).toInt());
    values.push_back(items.at(5).toInt());

    if(scenario.toInt() == 1) {
        this->load_scenario(":/scenario1.txt", offset.toInt(), values);
    }

    if(scenario.toInt() == 2) {
        this->load_scenario(":/scenario2.txt", offset.toInt(), values);
    }
    }
 }

 /**
 * @brief MainWindow destructor
 *
 * Deletes the UI
 *
 */
MainWindow::~MainWindow()
{
    delete ui;
}

/**
* @brief Loads a scenario from a text file
*
* Parses information about a set scenario from a properly formatted text file
* Each text file contains a set number of nodes, each of which contains information about a particular event in a story
* Parsed information is placed in a NodeDetails object in our nodes NodeDetails array
* We first parse the text for each decision and how they will affect each progress bar
* Then the directory for the corresponding image is parsed, followed by the dialogue text
* Then the corresponding sound files are stored and we set to which node each choice should lead the player
*/
void MainWindow::load_scenario(QString scenario, int offset, QVector<int>& values){
    QFile input_file(scenario);

    if(!input_file.open(QIODevice::ReadOnly|QIODevice::Text)){
        qDebug()<<"Error reading file."<<endl;
        this->close();
    }


    int node_counter = -1;
    int maxsize = 8;
    QString word = "", button1Text, button2Text, image;
    QStringList textValueVector1, textValueVector2;

    NodeDetails *nodes = new NodeDetails[maxsize];
    QTextStream in(&input_file);

    while(!in.atEnd()){

        word = in.readLine();
        if(word == "node"){
            node_counter++;
        }

        if(word != ""){
            image = in.readLine();
            button1Text = in.readLine();
            textValueVector1 = (in.readLine()).split(" ");
            button2Text = in.readLine();
            textValueVector2 = in.readLine().split(" ");
            cout<<button1Text.toStdString()<<endl;
        }

        if(node_counter<maxsize){
            cout << node_counter<<endl;
            nodes[node_counter].bg_image = image;
            nodes[node_counter].button1Text = button1Text;
            nodes[node_counter].button2Text = button2Text;

            for(int i = 0; i < textValueVector1.size(); i++) {
                nodes[node_counter].valueVector1.push_back(textValueVector1[i].toInt());
                nodes[node_counter].valueVector2.push_back(textValueVector2[i].toInt());
            }

        }

        word = in.readLine();
        if(word == "text"){
            word = in.readLine();
            while(word != "end"){
                nodes[node_counter].dialogue_text.push_back(word);
                cout<<"TEXT: "<<word.toStdString()<<endl;
                word = in.readLine();
            }
        }
    }

    cout<<"Done reading from file"<<endl;

    ////////////////////////////////////////////////////////////////////////////////////////
    //IMPORTANT: THE BELOW ASSUMES THE SOUND FILES ARE NAMED AS : NodeX_Y
    //WHERE X IS THE NODE NUMBER AND Y IS THE INDEX OF THE SOUND
    //SCENARIO FILES MUST BE NAMED AS : scenarioX.txt
    //WHERE X IS THE SCENARIO NUMBER
    QString directory = ":/sounds/scenario?";
    QString curr = scenario.split("/")[1].split(".")[0].split("scenario")[1];
    directory.replace("?", curr);

    QDirIterator *it = new QDirIterator(directory, QDirIterator::Subdirectories);
    while (it->hasNext()) {
        QString sf = it->next();
        nodes[sf.split("_")[0].split("/")[3].split("Node")[1].toInt()].sound_files.push_back(sf);
    }
    ////////////////////////////////////////////////////////////////////////////////////////

    for(int i = 0; i < maxsize; i++) {
        nodes[i].sound_files.sort();
    }

    nodes[0].choice1 = &nodes[1];
    nodes[0].choice2 = &nodes[2];
    nodes[1].choice1 = &nodes[3];
    nodes[1].choice2 = &nodes[3];
    nodes[2].choice1 = &nodes[3];
    nodes[2].choice2 = &nodes[3];
    nodes[3].choice1 = &nodes[4];
    nodes[3].choice2 = &nodes[4];
    nodes[4].choice1 = &nodes[5];
    nodes[4].choice2 = &nodes[6];
    nodes[5].choice1 = &nodes[7];
    nodes[5].choice2 = &nodes[6];
    //nodes[6].choice1 = &nodes[7];
    //nodes[6].choice2 = &nodes[7];
    nodes[6].choice1 = NULL;
    nodes[6].choice2 = NULL;
    nodes[7].choice1 = NULL;
    nodes[7].choice2 = NULL;

    story_node = new StoryNode(nodes + offset, this, values, user, curr.toInt(), nodes + offset);
    story_node->show();
    this->hide();
}

/**
* @brief Loads the first scenario
*
* Calls the load_scenario method with the path to the first scenario's directory
*
*/
void MainWindow::on_pushButton_3_clicked()
{
    QVector<int> values = {50, 50, 50, 50};
    load_scenario(":/scenario1.txt", 0, values);
}

/**
* @brief Loads the second scenario
*
* Calls the load_scenario method with the path to the second scenario's directory
*
*/
void MainWindow::on_pushButton_4_clicked()
{
    QVector<int> values = {50, 50, 50, 50};
    load_scenario(":/scenario2.txt", 0, values);
}

/**
* @brief Closes the window
*
*/
void MainWindow::on_pushButton_clicked()
{
    this->hide();
    parentWindow->show();
    this->close();
}



void MainWindow::on_pushButton_2_clicked()
{
    Info * info_window = new Info(this);
    this->hide();
    info_window->show();
}
