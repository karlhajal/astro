/**
 * @file playagain.h
 * @brief The PlayAgain class
 *
 * This class is for the statistics and suggestions at the end,
 * The data collected throughout a certain playthrough will be collected
 * and used to instantiate an instance of the PlayAgain class, which will
 * display the stats in a nice way and provide a way to teach the player for
 * bad choices he/she might have made,
 * It also gives the option to play again or go back to the main menu
 */

#ifndef PLAYAGAIN_H
#define PLAYAGAIN_H

#include <time.h>
#include <stdlib.h>
#include <QDialog>
#include <QLabel>
#include <vector>

namespace Ui {
    class PlayAgain;
}


class PlayAgain : public QDialog
{
    Q_OBJECT

public:
    explicit PlayAgain(QWidget *_main_menu=0, QWidget *parent=0,
                       int _happiness=0, int _adventure=0,
                       int _persistence=0, int _acceptance=0, int _decisiveness=0); /**< PlayAgain Constructor */
    ~PlayAgain(); /**< PlayAgain Desturctor */

private slots:
    void on_buttonBox_accepted(); /**< Restarts the scenario when a button is pressed*/

    void on_buttonBox_rejected(); /**< Goes back to main menu when a button is pressed*/

private:
    int happiness, adventure, acceptance, persistence, decisiveness; /**< values for the data collected throughout the playthrough */
    Ui::PlayAgain *ui; /**< A pointer to the forward declared class that Qt auto-generated from the ui file */
    QWidget *parent_widget; /**< Widget that instantiated this PlayAgain instance, should be StoryNode */
    QWidget *main_menu; /**< Main menu widget, used to go back to the main menu */
    void makeHyperLink(QLabel * myLabel); /**< A utility function which given a text link, transforms it into a hyper link for web use */

    std::vector<QString> happyVideos = { "https://www.youtube.com/watch?v=c0NGXyMTMvQ",
                                         "https://www.youtube.com/watch?v=5QvMDHIEX00:",
                                         "https://www.youtube.com/watch?v=NU6BpkTbM-A&t=8s" } /**< Vector of strings that link to online videos that help with happiness*/,
                         adventureVideos = { "https://www.youtube.com/watch?v=_VUIFaNxH-w",
                                             "https://www.youtube.com/watch?v=nIr7PqvbUUk",
                                             "https://www.youtube.com/watch?v=ZrWpTLuy-IQ" } /**< Vector of strings that link to online videos that help with adventureness*/,
                         acceptanceVideos = { "https://www.youtube.com/watch?v=Wl2_knlv_xw",
                                              "https://www.youtube.com/watch?v=Gc4HGQHgeFE",
                                              "https://www.youtube.com/watch?v=Fzn_AKN67oI" } /**< Vector of strings that link to online videos that help with acceptance*/,
                         persistenceVideos = { "https://www.youtube.com/watch?v=g-jwWYX7Jlo",
                                               "https://www.youtube.com/watch?v=CPQ1budJRIQ",
                                               "https://www.youtube.com/watch?v=gMFc7agO09w" } /**< Vector of strings that link to online videos that help with persistence*/,
                         decisivenessVideos = { "https://www.youtube.com/watch?v=jCxohnyfiBQ"
                                              } /**< Vector of strings that link to online videos that help with decisiveness*/;
};

#endif // PLAYAGAIN_H
