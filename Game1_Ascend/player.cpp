/**
* \file player.cpp
* \brief Player class method definition
*
*/

#include "player.h"
#include "globalparameters.h"

Player::Player(QObject *parent): QObject(parent)
{
    collisionTimer = new QTimer();
    movementTimer = new QTimer();
    this->setFlag(QGraphicsItem::ItemIsFocusable);
    this->setPixmap(QPixmap(":/images/character.png").scaled(GAME_SCENE_HEIGHT/8, GAME_SCENE_HEIGHT/8));
    speed = 4;
    satisfaction = 5;
    connect(collisionTimer, SIGNAL(timeout()), this, SLOT(checkCollision()));
    connect(movementTimer, SIGNAL(timeout()), this, SLOT(updateMovement()));
    collisionTimer->start(1);
    movementTimer->start(5);


    resetKeys();
}

void Player::resetKeys() {
    KeyBindings[down] = 0;
    KeyBindings[up] = 0;
    KeyBindings[right] = 0;
    KeyBindings[left] = 0;
}

void Player::keyPressEvent(QKeyEvent *event) {
    KeyBindings[event->key()] = 1;
}

void Player::keyReleaseEvent(QKeyEvent * event) {
    KeyBindings[event->key()] = 0;
}


void Player::updateMovement() {

    if(KeyBindings[down] && this->y() < GAME_SCENE_HEIGHT) {
        this->setPos(this->x(), this->y() + speed);
    }
    else if(KeyBindings[up] && this->y() > 0) {
        this->setPos(this->x(), this->y() - speed);
    }
    else if(KeyBindings[right] && this->x() < GAME_SCENE_WIDTH-50) {
        this->setPos(this->x() + speed, this->y());
    }
    else if(KeyBindings[left] && this->x() > 0) {
        this->setPos(this->x() - speed, this->y());
    }
}

//void Player::keyPressEvent(QKeyEvent *event) {
//    if(event->key() == Qt::Key_Down) {
//        this->setPos(this->x(), this->y() + speed);
//    }
//    else if(event->key() == Qt::Key_Up && this->y() > 20) {
//        this->setPos(this->x(), this->y() - speed);
//    }
//    else if(event->key() == Qt::Key_Right && this->x() < 1400) {
//        this->setPos(this->x() + speed, this->y());
//    }
//    else if(event->key() == Qt::Key_Left && this->x() > -20) {
//        this->setPos(this->x() - speed, this->y());
//    }
//    //if at the end position and with enough satisfaction -> win
//}

void Player::checkCollision() {
    QList<QGraphicsItem* > list = collidingItems();
    for(auto &u : list) {
        if(ValuePickup * v = dynamic_cast<ValuePickup *> (u)) {
            if(v->good) {
                satisfaction = std::min(satisfaction + 1, 5);
                goodValues.emplace_back(v->valuename);
            }
            else{
                satisfaction--;
                badValues.emplace_back(v->valuename);
            }
            //if satisfaction <= min amount -> Lose()
            this->scene()->removeItem(v);
            delete v;
        }
        else if(Bush * b = dynamic_cast<Bush *>(u)) {
            if(KeyBindings[right]) {
                this->setPos(this->x()- 40*speed, this->y());
            }
            if(KeyBindings[left]) {
                this->setPos(this->x() + 40*speed, this->y());
            }
            if(KeyBindings[down]) {
                this->setPos(this->x(), this->y() - 40*speed);
            }
            if(KeyBindings[up]) {
                this->setPos(this->x(), this->y() + 40*speed);
            }
        }
    }
}


