/**
* \file shalimonepickup.h
* \brief ShalimonePickup class
*
* Used to implement the pickups
*/

#ifndef SHALIMONEPICKUP_H
#define SHALIMONEPICKUP_H

#include <QObject>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QGraphicsPixmapItem>
#include <QPainter>
#include <QTimer>
#include <QThread>
#include "stdlib.h"
#include <QString>
#include <fstream>
#include "globalparameters.h"

class ShalimonePickup : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    explicit ShalimonePickup(QObject *parent = nullptr, int _speed = 0, QString _valuename = "", bool _good = true); //!< ShalimonePickup constructor

    int speed; //!< Movement speed
    bool good; //!< Specifies whether the pickup is good or bad
    QString valuename; //!< Contains the name of the trait represented by the pickup
signals:

public slots:
    void UpdatePosition(); //!< Updates the pickup's position to give it movement
};

#endif // SHALIMONEPICKUP_H
