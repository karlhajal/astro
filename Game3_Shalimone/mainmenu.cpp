/**
* \file mainmenu.cpp
* \brief MainMenu class method definition
*
*/

#include "mainmenu.h"
#include "ui_mainmenu.h"

MainMenu::MainMenu(QWidget *parent, QWidget* _parentWindow) :
    QWidget(parent),
    ui(new Ui::MainMenu)
{
    ui->setupUi(this);
    parentWindow = _parentWindow;

    shalim = new Sprite(0,":/images/idle.png", 40, 40, 200, 200, 17);

    bgtimer = new QTimer(this);
    connect(bgtimer, SIGNAL(timeout()), this, SLOT(updateBackground()));
    bgtimer->start(200);

    this->ui->image_label->setPixmap(shalim->getPixmap());
}

MainMenu::~MainMenu()
{
    delete ui;
}

void MainMenu::init() {

    scene = new ShalimoneScene(this, parentWindow);

    view = new QGraphicsView();
    view->setScene(scene);
    view->setFixedSize(SHALIMONE_SCENE_WIDTH,SHALIMONE_SCENE_HEIGHT);
    view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view->show();

}

void MainMenu::on_pushButton_2_clicked()
{
   init();
   this->hide();
}

void MainMenu::on_pushButton_clicked()
{
    this->hide();
    parentWindow->show();
    this->close();
}

void MainMenu::updateBackground(){

    this->ui->image_label->setPixmap(this->shalim->getPixmap());

}

void MainMenu::on_pushButton_3_clicked()
{
   info = new InfoWindow(nullptr, this);
   info->show();
   this->hide();
}
