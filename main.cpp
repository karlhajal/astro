/**
* \file main.cpp
* \brief The project's main file, used to run the application initially
*
*/

#include "Framework/loginform.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    LoginForm w;
    w.show();

    return a.exec();
}
