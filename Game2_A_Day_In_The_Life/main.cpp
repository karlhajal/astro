#include "mainwindow.h"
#include <QApplication>
#include <QFile>
#include <QString>


/**
 * @file main.cpp
 * @brief main , main entry function of the program
 * @param argc , command line argument count
 * @param argv , command line argument vector
 */
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    QFile File(":/stylesheet.qss");
    File.open(QFile::ReadOnly);
    QString StyleSheet = QLatin1String(File.readAll());

    //w.setStyleSheet(StyleSheet);

    return a.exec();
}
