/**
* \file signUp.h
* \brief signUp class
*
* Used to implement the new user sign up form
*/

#ifndef SIGNUP_H
#define SIGNUP_H

#include <QWidget>
#include <QtWidgets>
#include <fstream>
#include <QObject>

class signUp : public QWidget
{
    Q_OBJECT
public:
    explicit signUp(QWidget *parent = 0, QWidget* _parentWindow = nullptr); //!< signUp constructor
    QWidget* parentWindow;
    QVBoxLayout *VBL;
    QGridLayout *GL;
    QGroupBox * upperGrp = new QGroupBox;
    QLabel * FirstName, *LastName,* Email, *Password, *Username, *ConfirmPassword, *Age,* Gender;
    QLineEdit * LFirstName, *LLastName, *LEmail, *LPassword, *LUsername, *LConfirmPassword, *LAge, *LGender;
    QSpinBox * SBAge;
    QRadioButton *Male, *Female;
    QPushButton *Reset, *Submit, *back;

private:
    void setVerticalLayout();
    bool checkForm();
    bool checkFormEmpty();
    bool checkPasswordsMatch();
    bool checkPasswordValid();
    void setGridLayout();

signals:

public slots:
    void resetForm(); //!< Reset the form
    void goBack(); //!< Go back to previous menu
    void submitForm(); //!< Submit the form with current data
};

#endif // MAINWIDGET_H
