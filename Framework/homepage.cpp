/**
* \file homepage.cpp
* \brief Homepage class method definition
*
*/


#include "homepage.h"
#include "ui_homepage.h"
#include "Game1_Ascend/ascend.h"
#include "Game2_A_Day_In_The_Life/mainwindow.h"
#include "Game3_Shalimone/mainmenu.h"

Homepage::Homepage(QWidget *parent, QWidget* _parentWindow, QString user) :
    QDialog(parent),
    ui(new Ui::Homepage)
{
    ui->setupUi(this);
    parentWindow = _parentWindow;
    this->user = user;
}

Homepage::~Homepage()
{
    delete ui;
}

void Homepage::on_logout_button_clicked()
{
    this->hide();
    parentWindow->show();
}

void Homepage::on_pushButton_clicked()
{
    this->hide();
    Ascend* game = new Ascend(nullptr, this);
    game->show();
}

void Homepage::on_pushButton_2_clicked()
{
    this->hide();

    QFile save_file(user + "_2.txt");
    QString content;
    if (save_file.open(QIODevice::ReadOnly | QIODevice::Text))
        content = save_file.readAll();
    else
        content = "";

    MainWindow* w = new MainWindow(nullptr,this,content, user);
    w->show();
}

void Homepage::on_pushButton_3_clicked()
{
    this->hide();
    MainMenu* m = new MainMenu(nullptr, this);
    m->show();
}
