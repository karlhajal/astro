/**
* \file globalparameters.h
* \brief Global variables used throughout the games
*
*/

#ifndef GLOBALPARAMETERS_H
#define GLOBALPARAMETERS_H

const int SHALIMONE_SCENE_WIDTH = 1200; //!< Width of the scene
const int SHALIMONE_SCENE_HEIGHT = 800; //!< Height of the scene
const int PLATFORM_TIMER_FREQ = 10; //!< Frequency used for the platform timer
const int MOVEMENT_TIMER_FREQ = 5; //!< Frequency used to the movement timer
const int PLAYER_SIZE = 150; //!< Size of the player


const int PLATFORM_SPAWN_FREQ = 1500; //!< Frequency used to spawn platforms and pickups

const int PICKUP_SCORE = 5000; //!< Scored acquired from a pickup



const int PLATFORM_MAX_HEIGHT = 700; //!< Max height of the platform spawns
const int PLATFORM_SEPERATION = 100; //!< Seperation between platform spawn
const int PLATFORM_MIN_HEIGHT = 300; //!< Min height of the platform spawns


#endif // GLOBALPARAMETERS_H
