/**
* \file loginform.h
* \brief LoginForm class
*
* Simple UI window that's used to implement the Login form
*/


#ifndef LOGINFORM_H
#define LOGINFORM_H

#include <QDialog>

namespace Ui {
class LoginForm;
}

class LoginForm : public QDialog
{
    Q_OBJECT

public:
    explicit LoginForm(QWidget *parent = 0);
    ~LoginForm();

private slots:

    void on_login_button_clicked();

    void on_signup_button_clicked();

    void on_guest_button_clicked();

private:
    Ui::LoginForm *ui;
};

#endif // LOGINFORM_H
