/**
 * @file player.h
 * @brief The Player class
 *
 * Class that defines the player character's art, movement, collision and pickup collection
 */

#ifndef PLAYER_H
#define PLAYER_H

#include<QKeyEvent>
#include<QObject>
#include<QGraphicsRectItem>
#include<QTimer>
#include<QGraphicsScene>
#include<unordered_map>
#include<vector>
#include<QString>
#include "valuepickup.h"
#include "bush.h"

///Main class for the player.
/// \author Rami Awar
/// \author Karl Hajal
/// \author Elie Maamary
///
class Player : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
	///Constructor for the player class.
    explicit Player(QObject *parent = nullptr);

	///Checks for key presses.
    void keyPressEvent(QKeyEvent *event);

	///Checks for key releases.
    void keyReleaseEvent(QKeyEvent *event);

	///holds the currently pressed keys
    std::unordered_map<int, bool> KeyBindings;

    int up = Qt::Key_Up; ///< abbreviation for the up key
    int down = Qt::Key_Down; ///< abbreviation for the down key
    int left = Qt::Key_Left; ///< abbreviation for the left key
    int right = Qt::Key_Right; ///< abbreviation for the right key

    qreal speed; ///< speed of the player
    int satisfaction; ///< health of the player
    QTimer * collisionTimer; ///< timer that slots into checkCollisions();
    QTimer * movementTimer; ///< timer that slots into updateMovement();
    std::vector<QString> goodValues; ///< good values the player collected
    std::vector<QString> badValues; ///< bad values the player collected
    void resetKeys(); ///< reset all the keys to non-pressed

public slots:
     void checkCollision(); ///< checks collisions with bushes or pickups
     void updateMovement(); ///< checks for currently pressed keys
};

#endif // PLAYER_H
